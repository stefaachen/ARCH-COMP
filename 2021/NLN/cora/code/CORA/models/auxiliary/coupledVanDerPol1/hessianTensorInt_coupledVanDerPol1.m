function Hf=hessianTensorInt_coupledVanDerPol1(x,u)



 Hf{1} = interval(sparse(5,5),sparse(5,5));



 Hf{2} = interval(sparse(5,5),sparse(5,5));

Hf{2}(1,1) = -2*x(2);
Hf{2}(2,1) = -2*x(1);
Hf{2}(1,2) = -2*x(1);


 Hf{3} = interval(sparse(5,5),sparse(5,5));



 Hf{4} = interval(sparse(5,5),sparse(5,5));

Hf{4}(3,3) = -2*x(4);
Hf{4}(4,3) = -2*x(3);
Hf{4}(3,4) = -2*x(3);
