function [rom, proj] = ROM(fom,params,varargin)
% ROM - creates a reduced-order model of dimension r
%       caution: currently only autonomous systems supported!
%
% Syntax:  
%    rom = ROM(fom,r,method)
%
% Inputs:
%    fom - nonlinearSys object (full-order model = FOM)
%    params - model parameters (params.tFinal|R0|U)
%    method - order reduction method:
%               'POD': proper orthogonal decomposition,
%               'DEIM': discrete-empirical interpolation method,
%               'Q-DEIM': DEIM with QR factorization,
%               'Gappy': gappy POD
%    r - reduced dimension
%
% Outputs:
%    rom - function handle for ROM (reduced-order model = ROM)
%    proj - projection matrix (n x r) from/to subspace
%
% Example: 
%
% References:
%   [1] S. Chaturantabut, D. Sorensen. A state space error estimate for
%           POD-DEIM nonlinear model reduction, SIAM Journal on
%           Numerical Analysis 50(1), 2012, pp. 46-63.
%   [2] Z. Drmac, S. Gugercin. A new selection operator for the discrete
%           empirical interpolation method - improved a priori error bound
%           and extensions, SIAM Journal on Scientific Computing 38(2),
%           pp. A631-A648.
%   [3] R. Zimmermann, K. Willcox. An accelerated greedy missing point
%           estimation procedure, SIAM Journal on Scientific Computing
%           38(5), 2016, pp. A2827-A2850.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Mark Wetzlinger
% Written:       01-December-2020
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

% processing of input arguments -------------------------------------------

% default
method = 'POD';
r = -1; % for automated tuning below

methods = {'POD','DEIM','Q-DEIM','Gappy'};
% check if method is available
if nargin >= 3
    whichmethod = ismember(methods,varargin{1});
    if ~any(whichmethod)
        warning('Provided method not available. Default (POD) taken.');
    else
        method = methods{whichmethod};
    end
    if nargin == 4
        % check reduced dimension
        if ~isscalar(varargin{2})
            warning('Reduced dimension r is not a scalar number. Default taken.');
        elseif varargin{2} < 1 || varargin{2} >= fom.dim
            warning('Reduced dimension r not properly set. Default taken.');
        else
            r = varargin{2};
        end
    end
end

% -------------------------------------------------------------------------


% Preliminary step: separation into linear and nonlinear terms ------------
n = fom.dim;
xsym = sym('x',[n,1]);
fomsym = expand(fom.mFile(xsym));
% string of jacobian to facilitate separation
jac = jacobian(fomsym);
jacString = string(jac);
% 1. state not in diff. eq
noState = strcmp(jacString,"0");
% 2. states linear and nonlinear in diff. eq
nonlinState = contains(jacString,'x');
% 3. states only linear in diff. eq
linState = ~(noState | nonlinState);
% compute state matrix
A = double(subs(jac,xsym,zeros(n,1)));
A(~find(linState)) = 0;
% 4. states linear and nonlinear in diff. eq
mixedState = (A ~= 0) & nonlinState;
% update for 2.: now states only nonlinear in diff. eq
nonlinState = nonlinState & ~mixedState;
% sanity check
assert(sum(sum(noState + linState + mixedState + nonlinState)) == n^2,...
    'Faulty state identification');

% dimensions with mixed terms
mixedStateDims = any(mixedState,2); % logical indexing

% compute nonlinearity term by removing all linear terms
N = fomsym;
for i=1:n
    if ~mixedStateDims(i)
        linIdx = find(A(i,:));
        N(i) = subs(N(i),xsym(linIdx),zeros(length(linIdx),1));
    else
        % 1. remove all linear and mixed vars
        linIdx = find(A(i,:));
        N(i) = subs(N(i),xsym(linIdx),zeros(length(linIdx),1));
        % ... now, only nonlinear terms remain
        % 2. add back mixed terms (but without linear stuff)
        mixedStateinDim = find(mixedState(i,:));
        for j=1:length(mixedStateinDim)
            N(i) = N(i) + expand( xsym(mixedStateinDim(j)) * ...
                (jac(i,mixedStateinDim(j)) - A(i,mixedStateinDim(j))));
        end
    end
end
% -------------------------------------------------------------------------


% First step: Proper Orthogonal Decomposition -----------------------------
% (note: this step is mandatory for all methods)

% simulate center trajectory over equidistant time intervals
simparams.x0 = center(params.R0);
simparams.tFinal = params.tFinal;
nrOfSteps = 1000;
simparams.timeStep = simparams.tFinal / nrOfSteps;
[t,x] = simulate(fom,simparams);

% snapshot matrix
X = x';

% SVD of snapshot matrix
[U,S,V] = svd(X);
Sigma = diag(S(1:n,1:n));

% automated setting of reduced dimension r
if r == -1
    thr = 0.01; % 1 percent of energy lost
    r = find(cumsum(Sigma) / sum(Sigma) > 1-thr,1,'first');
end

% reduced-order basis vectors
Ur = U(:,1:r);
% write to output
proj = Ur;

if strcmp(method,'POD')
    % generate model file for ROM
    romhandle = createROMfile(fom,method,Ur,A,N);
    
elseif strcmp(method,'DEIM') || strcmp(method,'Q-DEIM') || strcmp(method,'Gappy')
    % generate model file of nonlinear part -------------------------------
    % path and filename for new models-file
    path = [coraroot filesep 'models' filesep 'Cora'];
    nonlinpartfilename = [fom.name '_nonlinpart'];

    % header syntax and comment
    funcStr = ['function dx = ' nonlinpartfilename '(x,u)'];
    header = ['% automatically generated file for only nonlinear parts in ''' ...
        fom.name ''' system' newline];

    % write down differential equation
    diffeqStr = '';
    for i=1:n
        diffeqStr = [diffeqStr 'dx(' num2str(i) ',1) = ' char(string(N(i))) ';' newline];
    end
    % replace x1 with x(1) etc.
    for j=1:n
        old = ['x' num2str(j)];
        new = ['x(' num2str(j) ')'];
        diffeqStr = strrep(diffeqStr,old,new);
    end
    
    % total text of model file
    totalStr = [funcStr newline header newline diffeqStr newline 'end']; 
    
    % open file - write - close
    fid = fopen([path filesep nonlinpartfilename '.m'],'w');
    fwrite(fid, totalStr);
    fclose(fid);
    % ---------------------------------------------------------------------
    
    % initialize nonlinear function
    nonlinpart = nonlinearSys(str2func(nonlinpartfilename),n,1);
    
    % simulate center trajectory over equidistant time intervals
    simparams.x0 = center(params.R0);
    simparams.tFinal = params.tFinal;
    nrOfSteps = 1000;
    simparams.timeStep = simparams.tFinal / nrOfSteps;
    [t,x] = simulate(nonlinpart,simparams);

    % snapshot matrix of nonlinear part
    Y = x';

    % SVD of snapshot matrix
    [W,L,Z] = svd(Y);
    Lambda = diag(L(1:n,1:n));
    
    % projection matrix
    if strcmp(method,'DEIM')
        % DEIM algorithm from [1, Alg. 1]
        s = r; % required to be well-defined
        P = zeros(n,1);
        [~, p] = max(abs(W(:,1)));                          % line 1
        P(p,1) = 1;                                         % line 2
        for i=2:s                                           % line 3
            temp1 = ( P'*W(:,1:i-1) ) \ ( P'*W(:,i) );      % line 4
            temp2 = W(:,i) - W(:,1:i-1)*temp1;              % line 5
            [~, p(i)] = max(abs(temp2));                    % line 6 / 8
            P(p(i),i) = 1;                                  % line 7
        end
        Ws = W(:,1:s);

    elseif strcmp(method,'Q-DEIM')
        % Q-DEIM algorithm from [2, bottom of page A639]
        s = r;
        Ws = W(:,1:s);
        [~,~,P] = qr(Ws');
        P = P(:,1:s);
    
    elseif strcmp(method,'Gappy')
        % greedy algorithm from [3, Alg. 1]
        P = [];
        Jsbar = 1:n;                                        % line 1
        s = r;  % ... choice of s is unclear
        Ws = W(:,1:s);
        for i=1:s
            sigopt = 0;                                     % line 1
            for j=1:length(Jsbar)                           % line 2
                idx = Jsbar(j);
                Ptilde = [P, double(1:n == idx)'];          % line 3
                [~,temp,~] = svd(Ptilde'*Ws);               % line 4...
                sigj = min(temp(temp ~= 0));                % ...end
                if sigj > sigopt                            % line 5
                    sigopt = sigj;                          % line 6
                    jopt = idx;                             % line 7
                end                                         % line 8           
            end                                             % line 9
            P = [P, double(1:n == jopt)'];
            Jsbar = Jsbar(Jsbar ~= jopt);
        end
    end
    
    % generate model file for ROM
    romhandle = createROMfile(fom,method,Ur,A,N,Ws,P);
    
end

% init rom system
rom = nonlinearSys(romhandle,r,1);

% -------------------------------------------------------------------------

end

%------------- END OF CODE --------------