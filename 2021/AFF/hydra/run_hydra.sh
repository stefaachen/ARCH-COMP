#!/bin/sh

# build container
echo "Building hydra. This may take a while."
docker build . -t hydra:latest

# run hydra
echo "Running hydra on selected benchmarks. The results will be output to stdout."
python measure_all.py >&1 | tee results_hydra.csv


