function Hf=hessianTensorInt_freeRigidBodyparamEq(x,u)



 Hf{1} = interval(sparse(6,6),sparse(6,6));

Hf{1}(3,2) = 3/10;
Hf{1}(2,3) = 3/10;


 Hf{2} = interval(sparse(6,6),sparse(6,6));

Hf{2}(3,1) = -8/5;
Hf{2}(1,3) = -8/5;


 Hf{3} = interval(sparse(6,6),sparse(6,6));

Hf{3}(2,1) = 5/2;
Hf{3}(1,2) = 5/2;
