function [Tf,ind] = thirdOrderTensorInt_SUTRACovid_ext(x,u)



 Tf{1,1} = interval(sparse(9,9),sparse(9,9));

Tf{1,1}(7,3) = -1;
Tf{1,1}(7,4) = -1;
Tf{1,1}(3,7) = -1;
Tf{1,1}(4,7) = -1;


 Tf{1,2} = interval(sparse(9,9),sparse(9,9));



 Tf{1,3} = interval(sparse(9,9),sparse(9,9));

Tf{1,3}(7,1) = -1;
Tf{1,3}(1,7) = -1;


 Tf{1,4} = interval(sparse(9,9),sparse(9,9));

Tf{1,4}(7,1) = -1;
Tf{1,4}(1,7) = -1;


 Tf{1,5} = interval(sparse(9,9),sparse(9,9));



 Tf{1,6} = interval(sparse(9,9),sparse(9,9));



 Tf{1,7} = interval(sparse(9,9),sparse(9,9));

Tf{1,7}(3,1) = -1;
Tf{1,7}(4,1) = -1;
Tf{1,7}(1,3) = -1;
Tf{1,7}(1,4) = -1;


 Tf{1,8} = interval(sparse(9,9),sparse(9,9));



 Tf{1,9} = interval(sparse(9,9),sparse(9,9));



 Tf{2,1} = interval(sparse(9,9),sparse(9,9));



 Tf{2,2} = interval(sparse(9,9),sparse(9,9));

Tf{2,2}(7,3) = -1;
Tf{2,2}(7,4) = -1;
Tf{2,2}(3,7) = -1;
Tf{2,2}(4,7) = -1;


 Tf{2,3} = interval(sparse(9,9),sparse(9,9));

Tf{2,3}(7,2) = -1;
Tf{2,3}(2,7) = -1;


 Tf{2,4} = interval(sparse(9,9),sparse(9,9));

Tf{2,4}(7,2) = -1;
Tf{2,4}(2,7) = -1;


 Tf{2,5} = interval(sparse(9,9),sparse(9,9));



 Tf{2,6} = interval(sparse(9,9),sparse(9,9));



 Tf{2,7} = interval(sparse(9,9),sparse(9,9));

Tf{2,7}(3,2) = -1;
Tf{2,7}(4,2) = -1;
Tf{2,7}(2,3) = -1;
Tf{2,7}(2,4) = -1;


 Tf{2,8} = interval(sparse(9,9),sparse(9,9));



 Tf{2,9} = interval(sparse(9,9),sparse(9,9));



 Tf{3,1} = interval(sparse(9,9),sparse(9,9));

Tf{3,1}(7,3) = 1;
Tf{3,1}(7,4) = 1;
Tf{3,1}(3,7) = 1;
Tf{3,1}(4,7) = 1;


 Tf{3,2} = interval(sparse(9,9),sparse(9,9));



 Tf{3,3} = interval(sparse(9,9),sparse(9,9));

Tf{3,3}(7,1) = 1;
Tf{3,3}(1,7) = 1;


 Tf{3,4} = interval(sparse(9,9),sparse(9,9));

Tf{3,4}(7,1) = 1;
Tf{3,4}(1,7) = 1;


 Tf{3,5} = interval(sparse(9,9),sparse(9,9));



 Tf{3,6} = interval(sparse(9,9),sparse(9,9));



 Tf{3,7} = interval(sparse(9,9),sparse(9,9));

Tf{3,7}(3,1) = 1;
Tf{3,7}(4,1) = 1;
Tf{3,7}(1,3) = 1;
Tf{3,7}(1,4) = 1;


 Tf{3,8} = interval(sparse(9,9),sparse(9,9));



 Tf{3,9} = interval(sparse(9,9),sparse(9,9));



 Tf{4,1} = interval(sparse(9,9),sparse(9,9));



 Tf{4,2} = interval(sparse(9,9),sparse(9,9));

Tf{4,2}(7,3) = 1;
Tf{4,2}(7,4) = 1;
Tf{4,2}(3,7) = 1;
Tf{4,2}(4,7) = 1;


 Tf{4,3} = interval(sparse(9,9),sparse(9,9));

Tf{4,3}(7,2) = 1;
Tf{4,3}(2,7) = 1;


 Tf{4,4} = interval(sparse(9,9),sparse(9,9));

Tf{4,4}(7,2) = 1;
Tf{4,4}(2,7) = 1;


 Tf{4,5} = interval(sparse(9,9),sparse(9,9));



 Tf{4,6} = interval(sparse(9,9),sparse(9,9));



 Tf{4,7} = interval(sparse(9,9),sparse(9,9));

Tf{4,7}(3,2) = 1;
Tf{4,7}(4,2) = 1;
Tf{4,7}(2,3) = 1;
Tf{4,7}(2,4) = 1;


 Tf{4,8} = interval(sparse(9,9),sparse(9,9));



 Tf{4,9} = interval(sparse(9,9),sparse(9,9));



 Tf{5,1} = interval(sparse(9,9),sparse(9,9));



 Tf{5,2} = interval(sparse(9,9),sparse(9,9));



 Tf{5,3} = interval(sparse(9,9),sparse(9,9));



 Tf{5,4} = interval(sparse(9,9),sparse(9,9));



 Tf{5,5} = interval(sparse(9,9),sparse(9,9));



 Tf{5,6} = interval(sparse(9,9),sparse(9,9));



 Tf{5,7} = interval(sparse(9,9),sparse(9,9));



 Tf{5,8} = interval(sparse(9,9),sparse(9,9));



 Tf{5,9} = interval(sparse(9,9),sparse(9,9));



 Tf{6,1} = interval(sparse(9,9),sparse(9,9));



 Tf{6,2} = interval(sparse(9,9),sparse(9,9));



 Tf{6,3} = interval(sparse(9,9),sparse(9,9));



 Tf{6,4} = interval(sparse(9,9),sparse(9,9));



 Tf{6,5} = interval(sparse(9,9),sparse(9,9));



 Tf{6,6} = interval(sparse(9,9),sparse(9,9));



 Tf{6,7} = interval(sparse(9,9),sparse(9,9));



 Tf{6,8} = interval(sparse(9,9),sparse(9,9));



 Tf{6,9} = interval(sparse(9,9),sparse(9,9));



 Tf{7,1} = interval(sparse(9,9),sparse(9,9));



 Tf{7,2} = interval(sparse(9,9),sparse(9,9));



 Tf{7,3} = interval(sparse(9,9),sparse(9,9));



 Tf{7,4} = interval(sparse(9,9),sparse(9,9));



 Tf{7,5} = interval(sparse(9,9),sparse(9,9));



 Tf{7,6} = interval(sparse(9,9),sparse(9,9));



 Tf{7,7} = interval(sparse(9,9),sparse(9,9));



 Tf{7,8} = interval(sparse(9,9),sparse(9,9));



 Tf{7,9} = interval(sparse(9,9),sparse(9,9));



 Tf{8,1} = interval(sparse(9,9),sparse(9,9));



 Tf{8,2} = interval(sparse(9,9),sparse(9,9));



 Tf{8,3} = interval(sparse(9,9),sparse(9,9));



 Tf{8,4} = interval(sparse(9,9),sparse(9,9));



 Tf{8,5} = interval(sparse(9,9),sparse(9,9));



 Tf{8,6} = interval(sparse(9,9),sparse(9,9));



 Tf{8,7} = interval(sparse(9,9),sparse(9,9));



 Tf{8,8} = interval(sparse(9,9),sparse(9,9));



 Tf{8,9} = interval(sparse(9,9),sparse(9,9));


 ind = cell(8,1);
 ind{1} = [1;3;4;7];


 ind{2} = [2;3;4;7];


 ind{3} = [1;3;4;7];


 ind{4} = [2;3;4;7];


 ind{5} = [];


 ind{6} = [];


 ind{7} = [];


 ind{8} = [];

