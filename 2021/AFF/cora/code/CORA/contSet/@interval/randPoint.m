function p = randPoint(obj,varargin)
% randPoint - computes random point in interval obj
%
% Syntax:  
%    p = randPoint(obj)
%    p = randPoint(obj,N)
%    p = randPoint(obj,N,type)
%    p = randPoint(obj,'all','extreme')
%
% Inputs:
%    obj - interval object
%    N - number of random points
%    type - type of the random point ('extreme' or 'normal')
%
% Outputs:
%    p - random point in interval
%
% Example: 
%    int = interval.generateRandom(2);
%    points = randPoint(int,20);
%
%    figure; hold on;
%    plot(int);
%    plot(points(1,:),points(2,:),'.k','MarkerSize',20);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/randPoint

% Author:       Mark Wetzlinger
% Written:      17-Sep-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    N = 1;
    type = 'normal';
    if nargin > 1 && ~isempty(varargin{1})
       N = varargin{1}; 
    end
    if nargin > 2 && ~isempty(varargin{2})
       type = varargin{2}; 
    end

    % get object properties
    c = center(obj); r = rad(obj); n = dim(obj);
    
    if isempty(obj)
        % empty set
        [msg,id] = errEmptySet(); error(id,msg);
    end

    % generate different types of extreme points
    if strcmp(type,'normal')
        
        p = c + (-1 + 2 * rand(length(r),N)) .* r;
        
        
    elseif strcmp(type,'extreme')
        
        % consider degenerate case
        ind = find(r > 0);
        if length(ind) < n
           obj = project(obj,ind);
           temp = randPoint(obj,N,type);
           p = c * ones(1,N);
           p(ind,:) = temp;
           return;
        end
        
        % return all extreme point
        if ischar(N) && strcmp(N,'all')
            
            p = vertices(obj);
            
        % generate random vertices
        elseif 10*N < 2^n
            
            p = zeros(n,N); cnt = 1;
            while cnt <= N
                temp = sign(-1 + 2*rand(n,1));
                if ~ismember(temp',p','rows')
                   p(:,cnt) = temp; cnt = cnt + 1; 
                end
            end
            p = c + p.*r;
            
        % select random vertices
        elseif N <= 2^n
            
            V = vertices(obj);
            ind = randperm(size(V,2));
            V = V(:,ind);
            p = V(:,1:N);
            
        % compute vertices and additional points on the boundary    
        else
            
            V = vertices(obj);
            p = [V, zeros(n,N-size(V,2))];
            
            for i = size(V,2)+1:N
               temp = sign(-1 + 2*rand(n,1));
               ind = randi([1,n]);
               temp(ind) = -1 + 2*rand();
               p(:,i) = c + temp .* r;
            end
        end
    else
        [msg,id] = errWrongInput('type');
        error(id,msg);
    end
end

%------------- END OF CODE --------------