function options = checkOptionsReachInner(obj,options)
% checkOptionsReachInner - checks if all necessary options are there and 
%                          have valid values
%
% Syntax:  
%    options = checkOptionsReachInner(obj,options)
%
% Inputs:
%    obj     - nonlinearSys object (unused)
%    options - options for nonlinearSys (1x1 struct)
%
% Outputs:
%    options - options for nonlinearSys (1x1 struct)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: reachInner

% Author:       Niklas Kochdumper
% Written:      14-Aug-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    checkName = 'checkOptionsReachInner';

    % check start and final time
    options = check_tStart_tFinal(obj, options, checkName);

    % check time step
    check_timeStep(options, obj, checkName);

    % check initial set
    check_R0(options, obj);
    
    % check input set
    options = check_inputSet(obj,options);
    
    % check zonotope order
    check_zonotopeOrder(options, obj);
    
    % check reduction technique
    options = check_reductionTechniqueUnderApprox(options, obj);

    % warnings for unused options (overdefined)
    validFields = {'tStart','tFinal','timeStep','R0','U','u', ...
                   'zonotopeOrder','reductionTechnique'};
    
    warning(printRedundancies(options,validFields));
end

% Auxiliary Functions -----------------------------------------------------

function options = check_reductionTechniqueUnderApprox(options, obj)
% check if the reduction technique for under-approximate reduction takes a
% correct value

    strct = 'options';
    option = 'reductionTechnique';
    defValue = 'sum';
    
    % the reductionTechnique has to match a predefined string
    validRedTech = {'sum','scale','linProg'};
    
    if ~isfield(options,option)
        options.reductionTechnique = defValue;
    elseif strcmp(options.reductionTechnique,defValue)
        % not necessary since default value
        printDefaultValue(obj,option,defValue);
    elseif ~any(strcmp(validRedTech,options.reductionTechnique))
        error(printOptionOutOfRange(obj,option,strct));
    end
end

%------------- END OF CODE --------------