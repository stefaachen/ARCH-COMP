%
%   plot_domain.m
%
%   created on: 01.01.2020
%       author: kaushik
%
%   this program plots the over and under-approximation of winning region
%
%   you need to run ./vehicle binary first
%   so that the controller bdds are generated
%   

% path of the SymbolicSet.m and the associated mex-file
addpath(genpath('../../../../'));

% load the controllers
% co = SymbolicSet('vehicle_winning_over.bdd');
cu0 = SymbolicSet('vehicle_controller_under_0.bdd');
cu1 = SymbolicSet('vehicle_controller_under_1.bdd');
% cw = SymbolicSet('vehicle_controller_wc.bdd');

% state space bounds 
lb = [0.0;0.0];
ub = [2.0;2.0];

% prepare the figure window
figure;
axis([lb(1) ub(1) lb(2) ub(2)]);
% view(-45,15);
hold on;


% % plot the over-approximation
% try
%     po = co.points;
%     scatter3(po(:,1),po(:,2),po(:,3),'.','MarkerFaceColor',[0.7 0.7 0.7],'MarkerEdgeColor',[0.7 0.7 0.7]);
% catch
%     warning('Points could not be loaded from controller_over.bdd\n');
% end

% plot the under-approximation
try
    pu = cu0.points;
    plot(pu(:,1),pu(:,2),'.','MarkerFaceColor',[0.9 0.9 0.9],'MarkerEdgeColor',[0.9 0.9 0.9]);
    pu = cu1.points;
    plot(pu(:,1),pu(:,2),'.','MarkerFaceColor',[0.9 0.9 0.9],'MarkerEdgeColor',[0.9 0.9 0.9]);
catch
    warning('Points could no be loaded from controller_under.bdd\n');
end

% plot the goals
% rectangle('Position', [0.3, 1.3, 0.4, 0.4], 'EdgeColor', 'g', 'LineWidth', 3);
rectangle('Position', [1.3, 0.3, 0.4, 0.4], 'EdgeColor', 'b', 'LineWidth', 3);
% G1 = SymbolicSet('G1.bdd','projection',[1 2]);
% plotCells(G1,'FaceColor','g');
% G2 = SymbolicSet('G2.bdd','projection',[1 2]);
% plotCells(G2,'FaceColor','b');
% plot the static obstacles
% rectangle('Position', [0, 0.8, 1.1, 0.1], 'EdgeColor', 'k', 'LineWidth', 3, 'FaceColor', 'k');
rectangle('Position', [1.1, 0.9, 0.1, 0.8], 'EdgeColor', 'k', 'LineWidth', 3, 'FaceColor', 'k');
rectangle('Position', [1.1, 1.7, 0.1, 0.3], 'EdgeColor', 'k', 'LineWidth', 3, 'FaceColor', 'k');
% position of the door
% rectangle('Position', [1.1, 0.9, 0.1, 0.8], 'EdgeColor', 'k', 'LineWidth', 3, 'LineStyle', '--');
rectangle('Position', [0, 0.8, 1.1, 0.1], 'EdgeColor', 'k', 'LineWidth', 3, 'LineStyle', '--');

% % plot the worst case controller domain
% try
%     pw = cw.points;
%     scatter3(pw(:,1),pw(:,2),pw(:,3),'.','MarkerFaceColor','r','MarkerEdgeColor','r');
% catch
%     warning('Points could no be loaded from controller_wc.bdd\n');
% end
savefig('vehicle_domain');