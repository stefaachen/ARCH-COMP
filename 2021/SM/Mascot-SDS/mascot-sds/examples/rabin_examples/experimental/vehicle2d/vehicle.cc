/*
 * vehicle.cc
 *
 *  created on: 01.02.2021
 *      author: kaushik
 */

/*
 * dubin's vehicle (sampled time) with bounded external noise
 *
 */

#include <array>
#include <iostream>
#include <math.h>

#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "TicToc.hh"
#include "RungeKutta4.hh"
#include "FixedPointRabin.hh"
#include "input.hh"
#include "Helper.hh"


/****************************************************************************/
/* main computation */
/****************************************************************************/
int main() {
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /* create a log file */
    std::freopen("vehicle.log", "w", stderr);
    std::clog << "vehicle.log" << '\n';
    
    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    scots::SymbolicSet ss(mgr,sDIM,lb,ub,eta);
    ss.addGridPoints();
    /* debug */
    ss.printInfo();
    ss.writeToFile("X.bdd");
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    scots::SymbolicSet is(mgr,iDIM,ilb,iub,ieta);
    is.addGridPoints();
    /* debug */
    is.writeToFile("U.bdd");
    
    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    scots::SymbolicSet sspost(ss,1); /* create state space for post variables */
    scots::SymbolicModelMonotonic<state_type,input_type> abstraction(&ss, &is, &sspost);
    if (RFF) {
        std::cout << "Reading transition relations from file." << '\n';
        scots::SymbolicSet mt(mgr,"maybeTransitions.bdd");
        scots::SymbolicSet st(mgr,"sureTransitions.bdd");
        abstraction.setTransitionRelations(mt.getSymbolicSet(),st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::clog << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile("maybeTransitions.bdd");
        (abstraction.getSureTransitions()).writeToFile("sureTransitions.bdd");
    }
    
    /* ********************* */
    /* The specification (GF (G1 V G2) & FG (G1 V G2)) using a 2-state Rabin automaton
     * state 1= kitchen or office
     * state 0= everything else */
    /* ********************** */
    size_t rabin_nof_states=9;
    /* the inputs:
        0-> kitchen or office,
        1-> other (everything else)  */
    scots::SymbolicSet kitchen(sspost), office(sspost), room1(sspost), room2(sspost), others(sspost), obstacle(sspost), i0(sspost), i1(sspost), i2(sspost), i3(sspost), i4(sspost), i5(sspost);
    kitchen.addPolytope(4,H2,g2,scots::INNER);
    office.addPolytope(4,H1,g1,scots::INNER);
    room1.addPolytope(4,H3,g3,scots::OUTER);
    room2.addPolytope(4,H4,g4,scots::INNER);
    others.addGridPoints();
    obstacle.addPolytope(4,H5,g5,scots::OUTER);
    room1.setSymbolicSet(room1.getSymbolicSet() & (!room2.getSymbolicSet()) & (!obstacle.getSymbolicSet()));
    others.setSymbolicSet(others.getSymbolicSet() & (!kitchen.getSymbolicSet()) & (!office.getSymbolicSet()) & (!room1.getSymbolicSet()) & (!room2.getSymbolicSet()) & (!obstacle.getSymbolicSet()));
    
    i0.setSymbolicSet(kitchen.getSymbolicSet());
    i1.setSymbolicSet(office.getSymbolicSet());
    i2.setSymbolicSet(room1.getSymbolicSet());
    i3.setSymbolicSet(room2.getSymbolicSet());
    i4.setSymbolicSet(others.getSymbolicSet());
    i5.setSymbolicSet(obstacle.getSymbolicSet());
    std::vector<BDD> rabin_inputs = {
        i0.getSymbolicSet(),
        i1.getSymbolicSet(),
        i2.getSymbolicSet(),
        i3.getSymbolicSet(),
        i4.getSymbolicSet(),
        i5.getSymbolicSet()
    };
    /* save rabin inputs */
    helper::checkMakeDir("rabin_inputs");
    i0.writeToFile("rabin_inputs/i0.bdd");
    i1.writeToFile("rabin_inputs/i1.bdd");
    i2.writeToFile("rabin_inputs/i2.bdd");
    i3.writeToFile("rabin_inputs/i3.bdd");
    i4.writeToFile("rabin_inputs/i4.bdd");
    i5.writeToFile("rabin_inputs/i5.bdd");
    /* the transitions */
    std::vector<std::array<size_t,3>> rabin_transitions ={
        {0, 0, 1},
        {0, 1, 0},
        {0, 2, 4},
        {0, 3, 0},
        {0, 4, 0},
        {0, 5, 7},
//
        {1, 0, 1},
        {1, 1, 2},
        {1, 2, 5},
        {1, 3, 1},
        {1, 4, 1},
        {1, 5, 7},
//
        {2, 0, 0},
        {2, 1, 0},
        {2, 2, 6},
        {2, 3, 3},
        {2, 4, 8},
        {2, 5, 7},
//
        {3, 0, 0},
        {3, 1, 0},
        {3, 2, 0},
        {3, 3, 0},
        {3, 4, 0},
        {3, 5, 7},
//
        {4, 0, 1},
        {4, 1, 0},
        {4, 2, 4},
        {4, 3, 0},
        {4, 4, 0},
        {4, 5, 7},
//
        {5, 0, 1},
        {5, 1, 2},
        {5, 2, 5},
        {5, 3, 1},
        {5, 4, 1},
        {5, 5, 7},
//
        {6, 0, 0},
        {6, 1, 0},
        {6, 2, 6},
        {6, 3, 3},
        {6, 4, 0},
        {6, 5, 7},
//
        {7, 0, 7},
        {7, 1, 7},
        {7, 2, 7},
        {7, 3, 7},
        {7, 4, 7},
        {7, 5, 7},
//
        {8, 0, 8},
        {8, 1, 8},
        {8, 2, 8},
        {8, 3, 3},
        {8, 4, 8},
        {8, 5, 7},
    };
    std::vector<std::array<std::vector<size_t>,2>> rabin_pairs;
    std::vector<size_t> v1 = {2};
    std::vector<size_t> v2 = {4,5,6,7};
    std::array<std::vector<size_t>,2> pair1 = {v1,v2};
    rabin_pairs.push_back(pair1);
    std::vector<size_t> v3 = {3};
    std::vector<size_t> v4 = {7};
    std::array<std::vector<size_t>,2> pair2 = {v3,v4};
    rabin_pairs.push_back(pair2);

    mascot::RabinAutomaton rabin(mgr, rabin_nof_states, sspost, rabin_inputs, rabin_transitions, rabin_pairs);
    /* save the rabin transition function */
    rabin.writeToFile("rabin");
    /* we setup a fixed point object to compute reach and stay controller */
    scots::FixedPointRabin fp(&abstraction,&rabin);
    std::cout << "FixedPoint initialized." << '\n';
    
    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    std::cout << "\n\n Starting computation of under-approximation of the a.s. controller.\n";
    timer.tic();
    std::vector<BDD> CU = fp.Rabin("under",accl_on,M,&rabin,verbose);
    /* there will be as many controllers as the number of states of the rabin automaton */
    scots::SymbolicSet controller_under(ss,is);
    helper::checkMakeDir("controllers");
    for (size_t i=1; i<=CU.size(); i++) {
        scots::SymbolicSet controller_under(ss,is);
        controller_under.setSymbolicSet(CU[i-1]);
        std::string Str = "";
        Str += "controllers/C";
        Str += std::to_string(i);
        Str += ".bdd";
        char Char[20];
        size_t Length = Str.copy(Char, Str.length() + 1);
        Char[Length] = '\0';
        controller_under.writeToFile(Char);
    }
    double time_under_approx = timer.toc();
    std::clog << "Time taken by the under-approximation computation: " << time_under_approx << "s.\n";
    
    return 1;
}
