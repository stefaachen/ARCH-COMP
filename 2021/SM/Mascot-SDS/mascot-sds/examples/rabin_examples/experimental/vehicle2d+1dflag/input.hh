/* Header file for inputs */
/* Created by: Kaushik */
/*      Date: 01.02.2021 */

#ifndef INPUT_HH_
#define INPUT_HH_

#include <float.h>

/* state space dim */
#define sDIM 3 /* 2 states and 1 flag for request */
#define iDIM 2

/* data types for the ode solver */
typedef std::array<double,sDIM> state_type;
typedef std::array<double,iDIM> input_type;
typedef std::array<double,2> state_xy_dim_type;

/* parallel or sequential implementation of computation of the
 transition functions */
const bool parallel = false;
int verbose = 2; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
/* read transitions from file flag */
const bool RFF = false;
const double rad_reach_boolean_dim = 0.25; /* the radius of the reachable set from the state dimensions 3 */
///* scaling */
//const double scale = 1;
/* sampling time */
const double tau = 0.08;
/* upper bounds on the noise which is considered to be centered around the origin */
const state_type W_ub = {0.08,0.08,0.0};
const double eps = DBL_MIN; /* a small constant accounting for the roundoff errors */

/* lower bounds of the hyper rectangle */
double lb[sDIM]={0.0,0.0,0.0};
/* upper bounds of the hyper rectangle */
double ub[sDIM]={2.0,2.0,1.0};
/* grid node distance diameter */
double eta[sDIM]={0.08,0.08,1.0};

/* input space parameters */
double ilb[iDIM]={-1.0,-1.0};
double iub[iDIM]={1.0,1.0};
double ieta[iDIM]={0.1,0.1};

/* goal states */
/* add inner approximation of P={ x | H x<= h } form state space */
/* office */
double H1[4*sDIM]={-1, 0, 0,
    1, 0, 0,
    0,-1, 0,
    0, 1, 0
};
double g1[4] = {-0.1,0.9,-1.1,1.9};
/* kitchen */
double H2[4*sDIM]={-1, 0, 0,
    1, 0, 0,
    0,-1, 0,
    0, 1, 0};
double g2[4] = {-1.1,1.9,-0.1,0.9};

/* request */
double Hr[2*sDIM]={0, 0, -1,
    0, 0, 1
};
double r1[2] = {-0.51,1.49}; /* active */
double r2[2] = {0.49,0.49}; /* inactive */

auto check_intersection = [](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
    bool is_intersection=true; /* boolean flag which is positive if there is an intersection */
    /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
    for (int i=0; i<2; i++) {
        if ((ub2[i]-lb1[i]<DBL_MIN) || (lb2[i]-ub1[i]>DBL_MIN)) {
            is_intersection=false;
            break;
        }
    }
    return is_intersection;
};

/* returns true if [lb2,ub2] is included in [lb1,ub1] */
auto check_inclusion = [](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
    bool is_included=true; /* boolean flag which is positive if there is an intersection */
    /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
    for (int i=0; i<2; i++) {
        if ((lb1[i]-lb2[i]>eps) || (ub2[i]-ub1[i]>eps)) {
            is_included=false;
            break;
        }
    }
    return is_included;
};

auto decomposition = [](input_type &u, state_type &z1, state_type &z2) -> state_type {
    state_type y;
    if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
        /* return the lower left corner of the reachable set approximation */
        y[0] = z1[0]+u[0]*tau;
        y[1] = z1[1]+u[1]*tau;
    } else { /* z1 is the upper bound and z2 is the lower bound */
        /* return the upper right corner of the reachable set approximation */
        y[0] = z1[0]+u[0]*tau;
        y[1] = z1[1]+u[1]*tau;
    }
//    if (abs(z1[2]+z2[2])<=eps) {
        /* if the request is inactive, it may appear in the next time step */
        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            /* return the lower left corner of the reachable set approximation */
            y[2] = 0.0-eps;
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            y[2] = 1.0+eps;
        }
//    } else if (abs(0.5*(z1[2]+z2[2])-1.0)<=eps) {
//        /* if the request is active, then it gets inactive only when the robot reaches the office */
//        /* the X-Y dimension of the corners of the current state */
//        state_xy_dim_type z1_xy_dim, z2_xy_dim;
//        z1_xy_dim[0]=z1[0];
//        z1_xy_dim[1]=z1[1];
//        z2_xy_dim[0]=z2[0];
//        z2_xy_dim[1]=z2[1];
//        /* the hyperrectangle representing the location of the office */
//        state_xy_dim_type lb2, ub2;
//        lb2[0]=-g1[0];
//        lb2[1]=-g1[2];
//        ub2[0]=g1[1];
//        ub2[1]=g1[3];
//        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
//            if (check_inclusion(lb2,ub2,z1_xy_dim,z2_xy_dim)) {
//                y[2]=0.0-rad_reach_boolean_dim;
//            } else {
//                y[2]=1.0-rad_reach_boolean_dim;
//            }
//        } else { /* z1 is the upper bound and z2 is the lower bound */
//            /* return the upper right corner of the reachable set approximation */
//            if ( check_inclusion(lb2,ub2,z2_xy_dim,z1_xy_dim)) {
//                y[2]=0.0+rad_reach_boolean_dim;
//            } else {
//                y[2]=1.0+rad_reach_boolean_dim;
//            }
//        }
//        
//    }

    return y;
};

#endif /* INPUT_HH_ */
