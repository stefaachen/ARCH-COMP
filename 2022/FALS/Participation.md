# Participation in ARCH-COMP 2022: Falsification Track

Contact: Gidon Ernst

Event: <https://cps-vo.org/group/ARCH/FriendlyCompetition>

## Getting Started

- Download/clone the benchmark models from the shared repostitory at <https://gitlab.com/goranf/ARCH-COMP/-/tree/master/models/FALS>
- The general benchmark setup is described in the report <https://www.easychair.org/publications/paper/F4kf>
- Reporting of results and validation is described in [Validation.md](Validation.md)

It is recommended to report initial results to the organizer early,
to sort out any misunderstandings or incompatibilities in the format.
Feel free to get in touch for any other question, too, after all, this is a friendly competition :)

## Important Changes in 2022

-   There is *no* maximal number of simulations that you can run.
    The intention is to have a more accurate account of *how* difficult the benchmarks are.
    So feel free to run as many simultions as you like, and definitely more than 300.

-   We aim to collect reports from *10 trials* instead of 50 trials to keep the overall simulation time low.

-   The wind turbine model is *removed* from the competition in 2022.
    It is too easy to be interesting for a comparison, and the overal experimental setup with selecting the wind from a few fixed profiles is not meaningful.

-   Two new report columns, *simulation time* and *total time*, described in the validation format.
    Alternatively, you can report the ratio *simulation time*/*total time* of the fraction of time spent inside simulation.

## Open Issues

-   The AT reference model is missing in the official repository, please refer to <https://gitlab.com/gernst/ARCH-COMP/-/tree/FALS/models/FALS/transmission> in the mean time
-   The F16 reference model seems to be broken currently
-   Documentation in the `requirements.txt` files may be outdated, please ignore these and refer to [Validation.md](Validation.md)
    or the [2021 report](https://www.easychair.org/publications/paper/F4kf)

Please report any further issue you come across, especially if the model files are too new for your Matlab version.

## Benchmark models

The benchmark models at <https://gitlab.com/goranf/ARCH-COMP/-/tree/master/models/FALS>
are given as a Simulink model together with two files to initialize and run the respective benchmark;
together with a script that tests a single run.
These are the means to run these specific models that are used as reference by the validator.

For a documentation of the format of the input/output traces, please refer to [Validation.md](Validation.md) document.
Note that F16 is the only benchmark that has *no* time varying input but a single parameter instead.

You are free to modify the benchmark models for your purpose, e.g. change the way the model is accessed by your falsification tool.
Please do not change the simulation settings (e.g. the ODE solver or the input port interpolation) to ensure consistency with what the validator uses.
It seems to be the case, however, that there are minor differences in running the models across different Matlab versions and across different computers.

## Requirement Formulas

The requirements are documented as formulas in Signal/Metric Temporal logic in [Validation.md](Validation.md) and
also in the [2021 report](https://www.easychair.org/publications/paper/F4kf) (in a nicely typeset form).

You should translate these requirements into your falsification tool appropriately (please take care to place parentheses correctly).
Please include in your report the mnenonic key that identifies the requirement, as well as the respective instance and model,
Falstar will then use its own representation of the requirement formula.

