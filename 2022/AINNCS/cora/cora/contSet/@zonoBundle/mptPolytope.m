function P = mptPolytope(zB)
% mptPolytope - Converts a zonotope bundle to a polytope
%
% Syntax:  
%    P = mptPolytope(zB)
%
% Inputs:
%    zB - zonoBundle object
%
% Outputs:
%    P - mptPolytope object
%
% Example: 
%    Z1 = zonotope(zeros(2,1),[1 0.5; -0.2 1]);
%    Z2 = zonotope(ones(2,1),[1 -0.5; 0.2 1]);
%    zB = zonoBundle({Z1,Z2});
%    P = mptPolytope(zB);
%
%    figure; hold on;
%    plot(zB,[1,2],'b');
%    plot(P,[1,2],'r--');
%
% Other m-files required: vertices, polytope
% Subfunctions: none
% MAT-files required: none
%
% See also: interval, vertices

% Author:       Niklas Kochdumper
% Written:      06-August-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

P = polytope(zB);
    
%------------- END OF CODE --------------