function varargout = norm(Z,varargin)
% norm - computes maximum norm value
%
% Syntax:  
%    val = norm(Z,type,mode)
%
% Inputs:
%    Z - zonotope object
%    type - (optional) which kind of norm (default: 2)
%    mode - (optional) 'exact', 'ub' (upper bound),'ub_convex' (more
%            precise upper bound computed from a convex program)
%
% Outputs:
%   val - norm value
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: minnorm

% Author:       Victor Gassmann
% Written:      31-July-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default values
mode = 'ub';
type = 2;

if nargin >= 2
    if ~(isnumeric(varargin{1}) || ischar(varargin{1}) ) ...
            || ( isnumeric(varargin{1}) && ~any(varargin{1} == [1,2,Inf]) ) ...
            || ( ischar(varargin{1}) && ~strcmp(varargin{1},'fro') )
        throw(CORAerror('CORA:wrongValue','second',"1, 2, Inf, or 'fro'"));
    end
    type = varargin{1};
end
if nargin >= 3
    if ~any(strcmp(varargin{2},{'exact','ub','ub_convex'}))
        throw(CORAerror('CORA:wrongValue','third',"'exact', 'ub', or 'ub_convex'"));
    end
    mode = varargin{2};
end


if strcmp(mode,'exact')
    [varargout{1:2}] = norm_exact(Z,type);
elseif strcmp(mode,'ub')
    Int = interval(Z);
    varargout{1} = norm(Int,type);
elseif strcmp(mode,'ub_convex')
    varargout{1} = norm_ub(Z,type);
end

%------------- END OF CODE --------------