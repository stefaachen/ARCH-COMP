function E = minus(E,S,varargin)
% plus - Overloaded '-' operator for approximating the Minkowski difference
%    of an ellipsoid as a minuend and a set as a subtrahend
%
% Syntax:  
%    E = minus(E,S)
%    E = minus(E,S,mode)
%    E = minus(E,S,L)
%    E = minus(E,S,L,mode)
%
% Inputs:
%    E - ellipsoid object
%    S - set representation/double matrix
%    L - (optional) directions to use for approximation
%    mode - (optional) type of approximation ('i': inner; 'o': outer)
%
% Outputs:
%    E - ellipsoid object after Minkowski difference
%
% Example: 
%    E1 = ellipsoid(10*eye(2));
%    E2 = ellipsoid(8*eye(2));
%    figure;hold on
%    plot(E1)
%    plot(E2)
%    plot(minus(E1,E2),[1,2],'r')
%
% References:
%   [1] Kurzhanskiy, A.A. and Varaiya, P., 2006, December. Ellipsoidal
%       toolbox (ET). In Proceedings of the 45th IEEE Conference on
%       Decision and Control (pp. 1498-1503). IEEE.
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      09-March-2021
% Last update:  04-July-2022 (VG: class array instead of cell array)
% Last revision:---

%------------- BEGIN CODE --------------
%% parsing & checking
% check input arguments
inputArgsCheck({{E,'att',{'ellipsoid'},{'scalar'}};
                {S,'att',{'contSet','numeric'},{''}}});

% check dims
equalDimCheck(E,S);

% parse arguments
if isempty(varargin)
    L = zeros(dim(E),0);
    mode = 'o';
elseif length(varargin)==1
    if isa(varargin{1},'char')
        mode = varargin{1};
        L = zeros(dim(E),0);
    elseif isa(varargin{1},'double')
        L = varargin{1};
        mode = 'o';
    else
        throw(CORAerror('CORA:wrongValue','third',"be of type 'double' or 'char'"));
    end
elseif length(varargin)==2
    if ~isa(varargin{1},'double')
        throw(CORAerror('CORA:wrongValue','third',"be of type 'double'"));
    end
    if ~isa(varargin{2},'char')
        throw(CORAerror('CORA:wrongValue','fourth',"be of type 'char'"));
    end
    L = varargin{1};
    mode = varargin{2};
else
    throw(CORAerror('CORA:tooManyInputArgs',4));
end


if all(isempty(S))
    return;
end

% check arguments
if ~any(mode==['i','o'])
    throw(CORAerror('CORA:wrongValue','fourth',"'i' or 'o'"));
end

if size(L,1)~=dim(E)
    throw(CORAerror('CORA:dimensionMismatch','obj1',E,'dim1',dim(E),...
        'obj2',L,'size2',size(L)));
end

%% different Minkowski differences
N = length(S);

if isa(S,'double')
    s = sum(S,2);
    E = ellipsoid(E.Q,E.q-s);
    return;
end

if isa(S,'ellipsoid')
    E = minusEllipsoid(E,S(1),L,mode);
    for i=2:N
        E = minusEllipsoid(E,S(i),L,mode);
    end
    return;
end

%------------- END OF CODE --------------