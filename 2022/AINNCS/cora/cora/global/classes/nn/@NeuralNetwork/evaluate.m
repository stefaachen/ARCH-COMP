function r = evaluate(obj, input, evParams)
% evaluate - compute the output of a neural network for the given input
%
% Syntax:
%    res = evaluate(obj,input)
%    res = evaluate(obj,input, evParams)
%
% Inputs:
%    obj - object of class neuralNetwork
%    input - input represented as a numeric or set
%    evParams - struct holding parameters for evaluation
%       .bound_approx: bool whether bounds should be overapproximated,
%                       or "sample" (not safe!)
%       .polynomial_approx: how non-linear layers should be approximated
%           - 'lin', 'quad', 'cub' or 'adaptive' (use NNActivationLayer.order)
%       .num_generators: max number of generators for order reduction
%       .add_approx_error_to_Grest: whether 'd' should be added to Grest
%       .plot_multi_layer_approx_info: plotting for NNApproximationLayer
%       .approx_type: how to find coefficients for 'adaptive'
%                       'regression', 'throw-catch'
%       .reuse_bounds: wheter bounds should be reused
%       .max_bounds: max order used in refinement
%       .do_pre_order_reduction: wheter to do a order reduction before
%                       evaluating the pZ using the polynomial
%       .max_gens_post: max num_generator before post order reduction
%       .remove_Grest: whether to restructue s.t. there remain no ind. gens
%       .force_approx_lin_at: (l, u) distance at which to use 'lin'
%                       instead of order using 'adaptive'
%
% Outputs:
%    res - output of the neural network
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork
%
% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% validate parameters
if nargin < 3
    evParams = struct;
end

% evParams
if ~isfield(evParams, "bound_approx")
    evParams.bound_approx = false;
elseif strcmp(evParams.bound_approx, "sample")
    warning("Choosing Bound estimation '%s' does not lead to safe verification!", evParams.bound_approx);
end
if ~isfield(evParams, "polynomial_approx")
    evParams.polynomial_approx = "lin";
elseif ~ismember(evParams.polynomial_approx, {'lin', 'quad', 'cub', 'adaptive'})
    [msg, id] = errWrongInput('polynomial_approx');
    error(msg, id);
end
if ~isfield(evParams, "num_generators")
    evParams.num_generators = [];
end
if ~isfield(evParams, "add_approx_error_to_Grest")
    evParams.add_approx_error_to_Grest = false;
end
if ~isfield(evParams, "plot_multi_layer_approx_info")
    evParams.plot_multi_layer_approx_info = false;
end
if ~isfield(evParams, "approx_type")
    evParams.approx_type = "regression";
end
if ~isfield(evParams, "reuse_bounds")
    evParams.reuse_bounds = false;
end
if ~isfield(evParams, "max_bounds")
    evParams.max_bounds = 5;
end
if ~isfield(evParams, "do_pre_order_reduction")
    evParams.do_pre_order_reduction = true;
elseif ~isa(evParams.do_pre_order_reduction, 'logical')
    error("'evParams.do_pre_order_reduction' must be logical.")
end
if ~isfield(evParams, "max_gens_post")
    evParams.max_gens_post = 100 * evParams.num_generators;
end
if ~isfield(evParams, "remove_Grest")
    evParams.remove_Grest = true;
elseif ~isa(evParams.remove_Grest, 'logical')
    error("'evParams.remove_Grest' must be logical.")
end
if ~isfield(evParams, "force_approx_lin_at")
    evParams.force_approx_lin_at = Inf;
end


% execute
if isnumeric(input)
    r = input;
    % evaluate numeric input
    for i = 1:length(obj.layers)
        evParams.i = i;
        layer_i = obj.layers{i};
        r = layer_i.evaluateNumeric(r);
    end

elseif isa(input, 'zonotope')
    % evaluate zonotope input
    Z = input.Z;
    for i = 1:length(obj.layers)
        evParams.i = i;
        layer_i = obj.layers{i};
        Z = layer_i.evaluateZonotope(Z, evParams);
    end
    r = zonotope(Z);

elseif isa(input, 'polyZonotope')
    % evaluate polyZonotope input
    % init values
    c = input.c;
    G = input.G;
    Grest = input.Grest;
    expMat = input.expMat;
    id = input.id;
    id_ = max(id);
    if isempty(G)
        G = zeros(size(c, 1), 0);
    end
    if isempty(Grest)
        Grest = zeros(size(c, 1), 0);
    end
    ind = find(prod(ones(size(input.expMat))-mod(input.expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(input.expMat, 2), ind);

    for i = 1:length(obj.layers)
        evParams.i = i;
        layer_i = obj.layers{i};
        [c, G, Grest, expMat, id, id_, ind, ind_] = layer_i.evaluatePolyZonotope(c, G, Grest, expMat, id, id_, ind, ind_, evParams);
    end

    r = polyZonotope(c, G, Grest, expMat, id);

elseif isa(input, 'taylm')
    % evaluate taylm input
    r = input;
    for i = 1:length(obj.layers)
        evParams.i = i;
        layer_i = obj.layers{i};
        r = layer_i.evaluateTaylm(r, evParams);
    end

elseif isa(input, 'conZonotope')
    % evaluate conZonotope input

    % convert constrained zonotope to star set
    [c, G, C, d, l, u] = NNHelper.conversionConZonoStarSet(input);

    % predefine options for linear programming for speed-up
    options = optimoptions('linprog', 'display', 'off');

    for i = 1:length(obj.layers)
        evParams.i = i;
        layer_i = obj.layers{i};
        [c, G, C, d, l, u] = layer_i.evaluateConZonotope(c, G, C, d, l, u, options, evParams);
    end
    % convert star set to constrained zonotope
    r = NNHelper.conversionStarSetConZono(c, G, C, d, l, u);

else
    error("This type of set representation is not supported yet: '%s'!", class(input));
end
end