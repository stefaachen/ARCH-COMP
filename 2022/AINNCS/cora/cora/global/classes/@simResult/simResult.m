classdef simResult
% simResult - class that stores simulation results
%
% Syntax:  
%    obj = simResult(x,t)
%    obj = simResult(x,t,loc)
%    obj = simResult(x,t,[],y)
%    obj = simResult(x,t,[],y,a)
%
% Inputs:
%    x - cell-array storing the states of the simulated trajectories, where
%        each trajectory is a matrix of dimension [N,n]
%    t - cell-array storing the time points for the simulated trajectories
%        as vectors of dimension [N,1]
%    loc - cell-array storing the locations for the simulated trajectories
%    y - cell-array storing the output of the simulated trajectories, where
%        each trajectory is a matrix of dimension [N,o]
%    a - cell-array storing the algebraic variables of the simulated
%        trajectories (only for nonlinDASys), where each trajectory is a
%        matrix of dimension [N,p]
%    (where N ... number of simulated trajectories, n ... state dimension,
%     o ... output dimension, p ... number of algebraic variables)
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: reachSet, simulateRandom

% Author:       Niklas Kochdumper
% Written:      29-May-2020
% Last update:  16-November-2021 (MW, add property .y)
%               02-June-2022 (MW, add property .a)
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    
    x (:,1) {cell} = {};     % states of the simulated trajectories
    y (:,1) {cell} = {};     % outputs of the simulated trajectories
    a (:,1) {cell} = {};     % algebraic variables of the sim. trajectories
    t (:,1) {cell} = {};     % time of the simulated trajectories
    loc (:,1) {cell} = {};   % index of the locations (hybrid systems only)
    
end


methods
    
    % class constructor
    function obj = simResult(varargin)
        
        % parse input arguments
        if nargin == 0
            % empty object
        elseif nargin == 1
            throw(CORAerror('CORA:notEnoughInputArgs',2));
        elseif nargin == 2
            obj.x = varargin{1};
            obj.t = varargin{2};
        elseif nargin == 3
            obj.x = varargin{1};
            obj.t = varargin{2};
            obj.loc = varargin{3};
        elseif nargin == 4
            obj.x = varargin{1};
            obj.t = varargin{2};
            obj.loc = varargin{3};
            obj.y = varargin{4};
        elseif nargin == 5
            obj.x = varargin{1};
            obj.t = varargin{2};
            obj.loc = varargin{3};
            obj.y = varargin{4};
            obj.a = varargin{5};
        else
        	throw(CORAerror('CORA:tooManyInputArgs',5));
        end
    end
end

end

%------------- END OF CODE --------------