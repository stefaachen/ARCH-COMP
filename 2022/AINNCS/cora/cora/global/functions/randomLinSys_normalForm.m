function linSys = randomLinSys_normalForm(n,realInterval,imagInterval)
% randomLinSys - generates a random linear system by randomly placing poles
%
% Syntax:  
%    linSys = randomLinSys(n,realInterval,imagInterval,type)
%
% Inputs:
%    n - dimension
%    realInterval - interval of real values of poles
%    imagInterval - interval of imag values of poles 
%    type - type of probability distribution
%
% Outputs:
%    linSys - linearSys object
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      11-February-2011
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%determine number of real and conjugate complex eigenvalues
if rem(n,2)>0
    nReal = 1;
else
    nReal = 0;
end
nConj = floor(n/2);


%create system matrix A
A = zeros(n);
for i=1:(n-1)
    for j=1:i
        A(n-i,n+1-j) = 0.03*(1-2*rand(1,1));
    end
end
for i=1:(n)
    A(i,i) = -3*rand(1,1)-0.2;
end

B = 2*rand(n)-1;

%instantiate linear system
linSys = linearSys('linSys',A,B);

%------------- END OF CODE --------------