dims = [2 10, 10, 20, 3];

layers = cell(1, 2*(length(dims)-1));

for i = 1:(length(dims)-1)
    % init weights [-1 1]
    W = rand(dims(i+1), dims(i)) * 2 - 1;
    b = rand(dims(i+1), 1) * 2 - 1;

    layers{2*i-1} = NNLinearLayer(W, b);
    layers{2*i} = NNReLULayer();
end

nn = NeuralNetwork(layers);
input = rand(dims(1), 1);
input = zonotope([0 1 1 0; ...
0 1 0 1]);

% TODO deal with input.Z !!
output = nn.evaluate(input);

disp(input)
disp("->")
disp(output.Z)
plot(output, [1 2])