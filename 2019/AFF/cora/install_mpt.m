
% create installation directory
mkdir('tbxmanager')
cd tbxmanager

% install toolbox-manager
urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');
tbxmanager
savepath

% install mpt-toolbox
tbxmanager install mpt mptdoc cddmex fourier glpkmex hysdel lcp sedumi espresso

% initialize toolbox
mpt_init
cd ..