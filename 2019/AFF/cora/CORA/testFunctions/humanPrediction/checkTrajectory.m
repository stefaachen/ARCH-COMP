function [t_violation_vel, t_violation_acc] = checkTrajectory(traj, s_0_delta, v_0_delta, v_max, a_max, t, horizon)
% checkTrajectory - checks whether a trajectory lies within the predicted
% region for a given time horizon. The prediction is started from each
% point in time of the recorded trajectory.
% 
%
% Syntax:  
%    t_violation = checkTrajectory(traj, s_0_delta, v_0_delta, a_max, t, horizon)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    s_0_delta - radius of initial position uncertainty
%    v_0_delta - radius of initial velocity uncertainty
%    v_max - maximum velocity
%    a_max - maximum acceleration
%    t - vector of points in time
%    horizon - prediction horizon
%
% Outputs:
%    t_violation - time relative to the start time of first violation; inf
%    is returned if no violation took place.
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      05-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% current time and final time
t_curr = t(1);
t_final = t(end);

% init violation
violation = 0;

% set time of violation
t_violation_vel = inf;
t_violation_acc = inf;

% init step 
step = 1;

% while no violation took place and current time is less than final time
while ~violation && t_curr < t_final
    
    % when should the check stop?
    t_stop = min(t_curr + horizon, t_final);
    % index of time vector when to stop
    step_stop = find(t>=t_stop, 1);
    % construct the local time for checking
    t_local = t(step:step_stop) - t(step);
    % local trajectory
    try
    traj_local = traj(step:step_stop,:);
    catch
        disp('stop')
    end
    
    % check partial trajectory
    [t_violation_vel, t_violation_acc] = checkPartialTrajectory(traj_local, s_0_delta, v_0_delta, v_max, a_max, t_local);
    
    % check violation
    if t_violation_vel < inf || t_violation_acc < inf
        violation = 1;
    end

    % increment step and update current time
    step = step + 1;
    t_curr = t(step);
end


%------------- END OF CODE --------------