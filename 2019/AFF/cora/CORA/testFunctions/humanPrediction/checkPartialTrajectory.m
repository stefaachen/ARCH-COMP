function [t_violation_vel, t_violation_acc] = checkPartialTrajectory(traj, s_0_delta, v_0_delta, v_max, a_max, t)
% checkPartialTrajectory - checks whether a partial trajectory of 
% predefined length lies within the predicted occupancy. 
% 
% Syntax:  
%    t_violation = checkPartialTrajectory(traj, s_0_delta, v_0_delta, a_max, t, horizon)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    s_0_delta - radius of initial position uncertainty
%    v_0_delta - radius of initial velocity uncertainty
%    v_max - maximum velocity
%    a_max - maximum acceleration
%    t - vector of points in time
%    horizon - prediction horizon
%
% Outputs:
%    t_violation - time relative to the start time of first violation; inf
%    is returned if no violation took place.
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      05-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% initial position
s_0 = traj(1,:);

% averaging window
window = min(3,length(t)-1);

% first averaged velocities
v = zeros(window,length(s_0));
for i = 1:window
    % velocity from differentiation
    v(i,:) = (traj(i+1,:) - traj(i,:))/(t(i+1) - t(i));
end

% average veocity
avgVel = sum(v)/window;

% set initial velocity
v_0 = avgVel;

% compute predicted occupancy
[B_vel, B_acc] = markerReach(s_0', v_0', s_0_delta,v_0_delta,v_max,a_max,t);

% set enclosure
enclosure = 1;

% set iStep
iStep = 1;

% set time of violation
t_violation_vel = inf;
t_violation_acc = inf;

% time steps
timeSteps = length(t);

while enclosure && iStep < timeSteps
    % check if current position is enclosed in velocity ball
    if ~containsPoint(B_vel{iStep},traj(iStep,:)')
        enclosure = 0;
        t_violation_vel = t(iStep);
    end
    % check if current position is enclosed in acceleration ball
    if ~containsPoint(B_acc{iStep},traj(iStep,:)')
        enclosure = 0;
        t_violation_acc = t(iStep);
    end
    %increment iStep
    iStep = iStep + 1;
end

%------------- END OF CODE --------------