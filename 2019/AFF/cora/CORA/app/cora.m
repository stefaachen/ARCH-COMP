%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Initialization code 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialization function
function varargout = cora(varargin)
% CORA MATLAB code for cora.fig
%      CORA, by itself, creates a new CORA or raises the existing
%      singleton*.
%
%      H = CORA returns the handle to a new CORA or the handle to
%      the existing singleton*.
%
%      CORA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CORA.M with the given input arguments.
%
%      CORA('Property','Value',...) creates a new CORA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cora_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cora_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cora

% Last Modified by GUIDE v2.5 07-Mar-2019 14:28:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cora_OpeningFcn, ...
                   'gui_OutputFcn',  @cora_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
               
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% Opening function --- Executes before cora is made visible
function cora_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;

WS_vars = evalin('base', 'who');
WS_vars{end+1} = '';

%show workspace variables
if isempty(WS_vars)
    set(handles.listWS_Linear, 'String', 'No Worskpace Variables')
    set(handles.popUTrans_Linear, 'String', 'No Workspace Variables')
    set(handles.popUTransVec_Linear, 'String', 'No Workspace Variables')
    set(handles.popX0_Sim_Linear, 'String', 'No Workspace Variables')  
    set(handles.popU_Sim_Linear, 'String', 'No Workspace Variables')  
else
    set(handles.listWS_Linear, 'String', WS_vars)
    set(handles.popUTrans_Linear, 'String', WS_vars)
    set(handles.popUTrans_Linear, 'Value', length(WS_vars))
    set(handles.popUTransVec_Linear, 'String', WS_vars)
    set(handles.popUTransVec_Linear, 'Value', length(WS_vars))
    set(handles.popX0_Sim_Linear, 'String', WS_vars)
    set(handles.popX0_Sim_Linear, 'Value', length(WS_vars))
    set(handles.popU_Sim_Linear, 'String', WS_vars)
    set(handles.popU_Sim_Linear, 'Value', length(WS_vars))
end

%show logo 
axes(handles.panelHeader, 'position', [0.6,0.3,0.6,0.7]);
cora_logo = imread('CoraLogo.bmp');
image(cora_logo)
axis off 
axis image

% Update handles structure
guidata(hObject, handles);

% Output Function --- Outputs are returned to the command line
function varargout = cora_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Custom functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function matrices = create_matrices_linear(handles)

if get(handles.checkA_Linear, 'Value')
    mat_str = get(handles.textA_Linear, 'String');
    A = evalin('base', mat_str);
    matrices.A = A;
end

if get(handles.checkB_Linear, 'Value')
    mat_str = get(handles.textB_Linear, 'String');
    B = evalin('base', mat_str);
    matrices.B = B;
end

if get(handles.checkC_Linear, 'Value')
    mat_str = get(handles.textC_Linear, 'String');
    C = evalin('base', mat_str);
    matrices.C = C;
end

if get(handles.checkD_Linear, 'Value')
    mat_str = get(handles.textD_Linear, 'String');
    D = evalin('base', mat_str);
    matrices.D = D;
end

if get(handles.checkE_Linear, 'Value')
    mat_str = get(handles.textE_Linear, 'String');
    E = evalin('base', mat_str);
    matrices.E = E;
end

if get(handles.checkF_Linear, 'Value')
    mat_str = get(handles.textF_Linear, 'String');
    F = evalin('base', mat_str);
    matrices.F = F;
end


function generate_file_linear(file_linear, handles)
id = fopen(file_linear, 'wt');

% ... write the system to the generated file
fprintf(id, '%s\n', '%System');

if get(handles.rbLoadSModel_Linear, 'Value')
    spaceEx_file = handles.spaceExFile;
    fprintf(id, '%s = ''%s'';\n', 'spaceEx_file', ...
            spaceEx_file);            
    fprintf(id, '%s;\n', 'spaceex2cora(spaceEx_file)');
elseif get(handles.rbCreateMat_Linear, 'Value')
    if get(handles.checkA_Linear, 'Value')
        mat_str = get(handles.textA_Linear, 'String');
        fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'A', mat_str);
        mats_str = {'A'};
    else
        error('A must be specified');
    end

    if get(handles.checkB_Linear, 'Value')
        mat_str = get(handles.textB_Linear, 'String');
        fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'B', mat_str);
        mats_str = join({mats_str{1},'B'}, ',');
    else
        error('B must be specified')
    end

    if get(handles.checkC_Linear, 'Value')
        mat_str = get(handles.textC_Linear, 'String');
        fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'C', mat_str);
        mats_str = join({mats_str{1},'C'}, ',');
    end

    if get(handles.checkD_Linear, 'Value')
        mat_str = get(handles.textD_Linear, 'String');
        fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'D', mat_str);
        mats_str = join({mats_str{1},'D'}, ',');
    end

    if get(handles.checkE_Linear, 'Value')
        mat_str = get(handles.textE_Linear, 'String');
        fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'E', mat_str);
        mats_str = join({mats_str{1},'E'}, ',');
    end

    if get(handles.checkF_Linear, 'Value')
        mat_str = get(handles.textF_Linear, 'String');
        fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'F', mat_str); 
        mats_str = join({mats_str{1},'F'}, ',');
    end

    fprintf(id, '%s = linearSys(''%s'', %s);\n\n', 'linSys', 'linearSys', ...
            mats_str{1});
end

% ... write the settings to the generated file
fprintf(id, '%s\n', '%Settings');

%tstart
tstart = get(handles.textTstart_Linear, 'String');
fprintf(id, '%s = %s;\n', 'options.tStart', tstart);

%tfinal
tfinal = get(handles.textTfinal_Linear, 'String');
fprintf(id, '%s = %s;\n', 'options.tFinal', tfinal);

%time step
time_step = get(handles.textTimeStep_Linear, 'String');
fprintf(id, '%s = %s;\n', 'options.timeStep', time_step);

%zonotope order
zOrder = get(handles.txtZOrder_Linear, 'String');
fprintf(id, '%s = %s;\n', 'options.zonotopeOrder', zOrder);

%taylor terms
taylor_terms = get(handles.txtTaylor_Linear, 'String');
fprintf(id, '%s = %s;\n', 'options.taylorTerms', taylor_terms);

%reduction technique
contents = cellstr(get(handles.popRT_Linear, 'String'));
RT = contents{get(handles.popRT_Linear, 'Value')};
fprintf(id, '%s = ''%s'';\n', 'options.reductionTechnique', RT); 

%linear algorithm
contents = cellstr(get(handles.popLA_Linear, 'String'));
linear_algorithm = contents{get(handles.popLA_Linear, 'Value')};
fprintf(id, '%s = %s;\n', 'options.linearAlgorithm', linear_algorithm);

%R0
if isfield(handles, 'R0')
    R0 = handles.R0;
    if ~isempty(R0)
        fprintf(id, '%s = %s;\n', 'options.R0', R0);
    else
        error('R0 is not defined')
    end
else
    error('R0 is not defined')
end

%U
if isfield(handles, 'U')
    U = handles.U;
    if ~isempty(U)
        fprintf(id, '%s = %s;\n', 'options.U', U);
    else
        error('U is not defined')
    end
else
    error('U is not defined')
end

%uTrans
if isfield(handles, 'uTrans')
    uTrans = handles.uTrans;
    fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'options.uTrans', uTrans);
end

%uTransVec
if isfield(handles, 'uTransVec')
    uTransVec = handles.uTransVec;
    fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'options.uTransVec', uTransVec);
end

%originContained
if get(handles.rbOriginYes_Linear, 'Value')
    fprintf(id, '%s = %s;\n\n', 'options.originContained', 'boolean(1)');
elseif get(handles.rbOriginNo_Linear, 'Value')
    fprintf(id, '%s = %s;\n\n', 'options.originContained', 'boolean(0)');
end

% ... write reachability analysis to the generated file
fprintf(id, '%s\n', '%Reachability');
fprintf(id, '%s = %s;\n\n', '[R, Rcont]', 'reach(linSys, options)');

% ... write the simulation to the generated file
fprintf(id, '%s\n', '%Simulation');

%u
if get(handles.rbSim_Linear, 'Value')
    if isfield(handles, 'u')
        u = handles.u;
        if ~isempty(u)
            fprintf(id, '%s = evalin(''base'', %s);\n', 'options.u', u);
        else
            error('u is not defined')
        end
    else
        error('u is not defined')
    end
    
    fprintf(id, '%s = evalin(''base'', ''%s'');\n', 'x0', handles.x0);
    fprintf(id, '%s = simulate(%s, %s, %s, %s, %s, %s);\n', ...
            'simRes', 'linSys', 'options', 'options.tstart', ...
            'options.tfinal', 'x0', 'options');
        
elseif get(handles.rbRSim_Linear, 'Value')
    fprintf(id, '%s = %s;\n', 'numberofpoints', ...
            get(handles.txtNoP_RSim_Linear, 'String'));
    fprintf(id, '%s = %s;\n', 'fractionvertices', ...
            get(handles.txtFV_RSim_Linear, 'String'));
    fprintf(id, '%s = %s;\n', 'fractioninputvertices', ...
            get(handles.txtFIV_RSim_Linear, 'String'));
    fprintf(id, '%s = %s;\n', 'inputchanges', ...
            get(handles.txtIC_RSim_Linear, 'String'));
    fprintf(id, '%s = simulate_random(%s, %s, %s, %s, %s, %s);\n', ...
            'simRes', 'linSys', 'options', 'numberofpoints', ...
            'fractionvertices', 'fractioninputvertices', 'inputchanges');

elseif get(handles.rbSimRRT_Linear, 'Value')
    fprintf(id, '%s = %s);\n', 'numberofpoints', ...
            get(handles.txtNoP_RSim_Linear, 'String'));
        
    if get(rbYesEPS_SimRRT_Linear, 'Value')
        fprintf(id, '%s = %s;\n', 'extremepointsampling', 'boolean(1)');
    elseif get(rbNoEPS_SimRRT_Linear, 'Value')
        fprintf(id, '%s = %s;\n', 'extremepointsampling', 'boolean(0)');
    end
    
    fprintf(id, '%s = %s);\n', 'stretchingfactor', ...
            get(handles.txtSF_SimRRT_Linear, 'String'));
    fprintf(id, '%s = simulate_rrt(%s, %s, %s, %s, %s, %s);\n', ...
            'simRes', 'linSys', 'options', 'numberofpoints', ...
            'Rcont', 'extremepointsampling', 'stretchingfactor');
end

fclose(id);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          General functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes during object creation, after setting all properties.
function popupMain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTE
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupMain.
function popupMain_Callback(hObject, eventdata, handles)
% hObject    handle to popupMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupMain contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupMain

contents = cellstr(get(hObject,'String'));
currentOption = contents{get(hObject,'Value')};

switch currentOption
    case 'Linear System'
        set(handles.panelLinear, 'visible', 'on')
        set(handles.panelNonLinear, 'visible', 'off')
        set(handles.panelHybrid, 'visible', 'off')
        set(handles.panelUnknown, 'visible', 'off')

    case 'Nonlinear System'
        set(handles.panelLinear, 'visible', 'off')
        set(handles.panelNonLinear, 'visible', 'on')
        set(handles.panelHybrid, 'visible', 'off')
        set(handles.panelUnknown, 'visible', 'off')

    case 'Hybrid System'
        set(handles.panelLinear, 'visible', 'off')
        set(handles.panelNonLinear, 'visible', 'off')
        set(handles.panelHybrid, 'visible', 'on')
        set(handles.panelUnknown, 'visible', 'off')

    case 'Unknown System'
        set(handles.panelLinear, 'visible', 'off')
        set(handles.panelNonLinear, 'visible', 'off')
        set(handles.panelHybrid, 'visible', 'off')
        set(handles.panelUnknown, 'visible', 'on')
end


% --- Executes on button press in pbRun.
function pbRun_Callback(hObject, eventdata, handles)
if ~isfield(handles, 'fileLinear')
    script = sprintf('%s.m', 'Linear');
    [file, path] = uiputfile(script);
    file_linear = fullfile(path, file);
    handles.fileLinear = file_linear;
    guidata(hObject, handles) %update handles
end

%generate the main script to execute
generate_file_linear(handles.fileLinear, handles)
run(handles.fileLinear)


% --- Execues on button press in pbSave.
function pbSave_Callback(hObject, eventdata, handles)
%if get(handles.rbLoadSModel_Linear, 'Value')
if ~isfield(handles, 'fileLinear')
    script = sprintf('%s.m', 'Linear');
    [file, path] = uiputfile(script);
    file_linear = fullfile(path, file);
    handles.fileLinear = file_linear;
    guidata(hObject, handles); %update handles
end

generate_file_linear(handles.fileLinear, handles)

if ~isfield(handles, 'dataLinear')
    zipfile = sprintf('%s.zip', 'data_linear');
    [file, path] = uiputfile(zipfile);
    data_linear_zip = fullfile(path, file);
    handles.dataLinear = data_linear_zip;
    guidata(hObject, handles);
end

if get(handles.rbLoadSModel_Linear, 'Value')
    zip(handles.dataLinear, {handles.spaceExFile, handles.fileLinear})
elseif get(handles.rbCreateMat_Linear, 'Value')
    matrices = create_matrices_linear(handles);
    save('matrices_linear', '-struct', 'matrices');
    zip(handles.dataLinear, {'matrices_linear.mat', handles.fileLinear});
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Linear - System 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in pbSystem.
function pbSystem_Callback(hObject, eventdata, handles)
% hObject    handle to pbSystem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.panelSystem_Linear, 'visible', 'on')
set(handles.panelSettings_Linear, 'visible', 'off')
set(handles.panelSimulation_Linear, 'visible', 'off')
set(handles.panelPlots_Linear, 'visible', 'off')

set(hObject, 'Enable', 'off')
set(handles.pbSimulation, 'Enable', 'on')
set(handles.pbSettings, 'Enable', 'on')
set(handles.pbPlots, 'Enable', 'on')


% --- Executes on button press in rbLoadSModel_Linear.
function rbLoadSModel_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbLoadSModel_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbLoadSModel_Linear

if get(hObject, 'Value')
    set(handles.pbLoadSModel_Linear, 'Enable', 'on')
    set(hObject, 'Enable', 'off')
    set(handles.rbCreateMat_Linear, 'Enable', 'on')
    set(handles.rbCreateMat_Linear, 'Value', 0)

    set(handles.checkA_Linear, 'Enable', 'off')
    set(handles.checkB_Linear, 'Enable', 'off')
    set(handles.checkC_Linear, 'Enable', 'off')
    set(handles.checkD_Linear, 'Enable', 'off')
    set(handles.checkE_Linear, 'Enable', 'off')
    set(handles.checkF_Linear, 'Enable', 'off')

    set(handles.checkA_Linear, 'Value', 0)
    set(handles.checkB_Linear, 'Value', 0)
    set(handles.checkC_Linear, 'Value', 0)
    set(handles.checkD_Linear, 'Value', 0)
    set(handles.checkE_Linear, 'Value', 0)
    set(handles.checkF_Linear, 'Value', 0)

    set(handles.textA_Linear, 'Enable', 'off')
    set(handles.textB_Linear, 'Enable', 'off')
    set(handles.textC_Linear, 'Enable', 'off')
    set(handles.textD_Linear, 'Enable', 'off')
    set(handles.textE_Linear, 'Enable', 'off')
    set(handles.textF_Linear, 'Enable', 'off')
end


% --- Executes on button press in rbCreateMat_Linear.
function rbCreateMat_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbCreateMat_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbCreateMat_Linear
 if get(hObject, 'Value')
     set(hObject, 'Enable', 'off')
     set(handles.rbLoadSModel_Linear, 'Enable', 'on')
     set(handles.rbLoadSModel_Linear, 'Value', 0)
     set(handles.pbLoadSModel_Linear, 'Enable', 'off')
     set(handles.checkA_Linear, 'Enable', 'on')
     set(handles.checkB_Linear, 'Enable', 'on')
     set(handles.checkC_Linear, 'Enable', 'on')
     set(handles.checkD_Linear, 'Enable', 'on')
     set(handles.checkE_Linear, 'Enable', 'on')
     set(handles.checkF_Linear, 'Enable', 'on')
 end

 
% --- Executes on button press in pbLoadSModel_Linear.
function pbLoadSModel_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbLoadSModel_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[spaceEx_file, spaceEx_path] = uigetfile('*.xml');      
set(hObject, 'string', spaceEx_file)
handles.spaceExFile = fullfile(spaceEx_path, spaceEx_file);
guidata(hObject, handles) %update handles


% --- Executes during object creation, after setting all properties.
function textA_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textA_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function textA_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textA_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
 

% --- Executes on button press in checkA_Linear.
function checkA_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to checkA_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkA_Linear
    
if get(hObject, 'Value')
    set(handles.textA_Linear, 'Enable', 'on')
else
    set(handles.textA_Linear, 'Enable', 'off')
end
 

% --- Executes during object creation, after setting all properties.
function textB_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textB_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function textB_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textB_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkB_Linear.
function checkB_Linear_Callback(hObject, eventdata, handles)

if get(hObject, 'Value')
    set(handles.textB_Linear, 'Enable', 'on')
else
    set(handles.textB_Linear, 'Enable', 'off')
end


% --- Executes during object creation, after setting all properties.
function textC_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textC_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function textC_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textC_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkC_Linear.
function checkC_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to checkC_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkC_Linear
if get(hObject, 'Value')
    set(handles.textC_Linear, 'Enable', 'on')
else
    set(handles.textC_Linear, 'Enable', 'off')
end


% --- Executes during object creation, after setting all properties.
function textD_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textD_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function textD_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textD_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkD_Linear.
function checkD_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to checkD_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkD_Linear
 if get(hObject, 'Value')
    set(handles.textD_Linear, 'Enable', 'on')
else
    set(handles.textD_Linear, 'Enable', 'off')
 end

    
% --- Executes during object creation, after setting all properties.
function textE_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textF_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function textE_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textB_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkE_Linear.
function checkE_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to checkE_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkE_Linear
 if get(hObject, 'Value')
    set(handles.textE_Linear, 'Enable', 'on')
else
    set(handles.textE_Linear, 'Enable', 'off')
 end


% --- Executes during object creation, after setting all properties.
function textF_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textF_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function textF_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textF_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in checkF_Linear.
function checkF_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to checkF_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkF_Linear
 if get(hObject, 'Value')
    set(handles.textF_Linear, 'Enable', 'on')
else
    set(handles.textF_Linear, 'Enable', 'off')
 end

 
% --- Executes during object creation, after setting all properties.
function listWS_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listWS_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
 

% --- Executes on selection change in listWS_Linear.
function listWS_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to listWS_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textB_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textB_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Linear - Settings 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in pbSimulation.
function pbSettings_Callback(hObject, eventdata, handles)
% hObject    handle to pbSimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.panelSystem_Linear, 'visible', 'off')
set(handles.panelSettings_Linear, 'visible', 'on')
set(handles.panelSimulation_Linear, 'visible', 'off')
set(handles.panelPlots_Linear, 'visible', 'off')

set(hObject, 'Enable', 'off')
set(handles.pbSystem, 'Enable', 'on')
set(handles.pbSimulation, 'Enable', 'on')
set(handles.pbPlots, 'Enable', 'on')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Linear - Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in pbSimulation.
function pbSimulation_Callback(hObject, eventdata, handles)
% hObject    handle to pbSimulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.panelSystem_Linear, 'visible', 'off')
set(handles.panelSettings_Linear, 'visible', 'off')
set(handles.panelSimulation_Linear, 'visible', 'on')
set(handles.panelPlots_Linear, 'visible', 'off')

set(hObject, 'Enable', 'off')
set(handles.pbSystem, 'Enable', 'on')
set(handles.pbSettings, 'Enable', 'on')
set(handles.pbPlots, 'Enable', 'on')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Linear - plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
% --- Executes on button press in pbPlots.
function pbPlots_Callback(hObject, eventdata, handles)
% hObject    handle to pbPlots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.panelSystem_Linear, 'visible', 'off')
set(handles.panelSettings_Linear, 'visible', 'off')
set(handles.panelSimulation_Linear, 'visible', 'off')
set(handles.panelPlots_Linear, 'visible', 'on')

set(hObject, 'Enable', 'off')
set(handles.pbSystem, 'Enable', 'on')
set(handles.pbSimulation, 'Enable', 'on')
set(handles.pbSettings, 'Enable', 'on')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                          Finish Linear
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%
    



    

    

% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


















     
     




% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton25.
function pushbutton25_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





function textTstart_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textTstart_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of textTstart_Linear as text
%        str2double(get(hObject,'String')) returns contents of textTstart_Linear as a double


% --- Executes during object creation, after setting all properties.
function textTstart_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textTstart_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function textTfinal_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textTfinal_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of textTfinal_Linear as text
%        str2double(get(hObject,'String')) returns contents of textTfinal_Linear as a double


% --- Executes during object creation, after setting all properties.
function textTfinal_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textTfinal_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function textTimeStep_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to textTimeStep_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of textTimeStep_Linear as text
%        str2double(get(hObject,'String')) returns contents of textTimeStep_Linear as a double


% --- Executes during object creation, after setting all properties.
function textTimeStep_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textTimeStep_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function txtZOrder_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtZOrder_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtZOrder_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtZOrder_Linear as a double
value = str2double(get(hObject, 'String'));

if value < 1
    value = 1;
end

value = num2str(value);
set(hObject, 'String', value);


% --- Executes during object creation, after setting all properties.
function txtZOrder_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtZOrder_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbDownZOrder_Linear.
function pbDownZOrder_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbDownZOrder_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtZOrder_Linear, 'String');
value = str2double(value) - 1;

if value > 0
    value = num2str(value);
    set(handles.txtZOrder_Linear, 'String', value);
end


% --- Executes on button press in pbUpZOrder_Linear.
function pbUpZOrder_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbUpZOrder_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtZOrder_Linear, 'String');
value = str2double(value) + 1;
value = num2str(value);
set(handles.txtZOrder_Linear, 'String', value);


function txtTaylor_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtTaylor_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtTaylor_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtTaylor_Linear as a double
value = str2double(get(hObject, 'String'));
if value < 1
    value = 1;
end

value = num2str(value);
set(hObject, 'String', value);



% --- Executes during object creation, after setting all properties.
function txtTaylor_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtTaylor_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbUpTaylor_Linear.
function pbUpTaylor_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbUpTaylor_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtTaylor_Linear, 'String');
value = str2double(value) + 1;
value = num2str(value);
set(handles.txtTaylor_Linear, 'String', value);


% --- Executes on button press in pbDownTaylor_Linear.
function pbDownTaylor_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbDownTaylor_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtTaylor_Linear, 'String');
value = str2double(value) - 1;

if value > 0
    value = num2str(value);
    set(handles.txtTaylor_Linear, 'String', value);
end


% --- Executes on selection change in popRT_Linear.
function popRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to popRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popRT_Linear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popRT_Linear


% --- Executes during object creation, after setting all properties.
function popRT_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popLA_Linear.
function popLA_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to popLA_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popLA_Linear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popLA_Linear


% --- Executes during object creation, after setting all properties.
function popLA_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popLA_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbInitialSet_Linear.
function pbInitialSet_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbInitialSet_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
U = input_set();
handles.U = U;
guidata(hObject, handles)


% --- Executes on button press in pbInputSet_Linear.
function pbInputSet_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbInputSet_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%run('input_set.m')
%input_set_h = findobj('Tag','input_set');
%input_set_handles = guidata(input_set_h);
R0 = input_set();
handles.R0 = R0;
guidata(hObject, handles)


% --- Executes on selection change in popUTrans_Linear.
function popUTrans_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to popUTrans_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popUTrans_Linear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popUTrans_Linear
contents = cellstr(get(hObject,'String'));
uTrans = contents{get(hObject,'Value')};
set(handles.txtUTrans_Linear,'String', uTrans);
handles.uTrans = uTrans;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popUTrans_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popUTrans_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popUTransVec_Linear.
function popUTransVec_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to popUTransVec_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popUTransVec_Linear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popUTransVec_Linear
contents = cellstr(get(hObject,'String'));
uTransVec = contents{get(hObject,'Value')};
set(handles.txtUTransVec_Linear,'String', uTransVec);
handles.uTransVec = uTransVec;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popUTransVec_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popUTransVec_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox15.
function checkbox15_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox15


% --- Executes on button press in rbSim_Linear.
function rbSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbSim_Linear
set(hObject, 'Enable', 'off')
set(handles.rbRSim_Linear, 'Enable', 'on')
set(handles.rbRSim_Linear, 'Value', 0)
set(handles.rbSimRRT_Linear, 'Enable', 'on')
set(handles.rbSimRRT_Linear, 'Value', 0)

set(handles.txtX0_Sim_Linear, 'Enable', 'on')
set(handles.txtU_Sim_Linear, 'Enable', 'on')
set(handles.popX0_Sim_Linear, 'Enable', 'on')
set(handles.popU_Sim_Linear, 'Enable', 'on')
set(handles.txtNoP_RSim_Linear, 'Enable', 'off')
set(handles.pbUpNoP_RSim_Linear, 'Enable', 'off')
set(handles.pbDownNoP_RSim_Linear, 'Enable', 'off')
set(handles.txtFV_RSim_Linear, 'Enable', 'off')
set(handles.txtFIV_RSim_Linear, 'Enable', 'off')
set(handles.txtIC_RSim_Linear, 'Enable', 'off')
set(handles.pbUpIC_RSim_Linear, 'Enable', 'off')
set(handles.pbDownIC_RSim_Linear, 'Enable', 'off')
set(handles.txtNoP_SimRRT_Linear, 'Enable', 'off')
set(handles.pbUpNoP_SimRRT_Linear, 'Enable', 'off')
set(handles.pbDownNoP_SimRRT_Linear, 'Enable', 'off')
set(handles.rbYesEPS_SimRRT_Linear, 'Enable', 'off')
set(handles.rbNoEPS_SimRRT_Linear, 'Enable', 'off')
set(handles.txtSF_SimRRT_Linear, 'Enable', 'off')


function txtX0_Sim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtX0_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtX0_Sim_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtX0_Sim_Linear as a double

WS_vars = evalin('base', 'who');
WS_vars{end+1} = '';

x0 = get(hObject, 'String');
handles.x0 = x0;
guidata(hObject, handles);
set(handles.popX0_Sim_Linear, 'Value', length(WS_vars))


% --- Executes during object creation, after setting all properties.
function txtX0_Sim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtX0_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtU_Sim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtU_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtU_Sim_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtU_Sim_Linear as a double
WS_vars = evalin('base', 'who');
WS_vars{end+1} = '';

u = get(hObject, 'String');
handles.u = u;
guidata(hObject, handles);
set(handles.popU_Sim_Linear, 'Value', length(WS_vars))


% --- Executes during object creation, after setting all properties.
function txtU_Sim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtU_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in rbRSim_Linear.
function rbRSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbRSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbRSim_Linear
set(hObject, 'Enable', 'off')
set(handles.rbSim_Linear, 'Enable', 'on')
set(handles.rbSim_Linear, 'Value', 0)
set(handles.rbSimRRT_Linear, 'Enable', 'on')
set(handles.rbSimRRT_Linear, 'Value', 0)

set(handles.txtX0_Sim_Linear, 'Enable', 'off')
set(handles.txtU_Sim_Linear, 'Enable', 'off')
set(handles.popX0_Sim_Linear, 'Enable', 'off')
set(handles.popU_Sim_Linear, 'Enable', 'off')
set(handles.txtNoP_RSim_Linear, 'Enable', 'on')
set(handles.pbUpNoP_RSim_Linear, 'Enable', 'on')
set(handles.pbDownNoP_RSim_Linear, 'Enable', 'on')
set(handles.txtFV_RSim_Linear, 'Enable', 'on')
set(handles.txtFIV_RSim_Linear, 'Enable', 'on')
set(handles.txtIC_RSim_Linear, 'Enable', 'on')
set(handles.pbUpIC_RSim_Linear, 'Enable', 'on')
set(handles.pbDownIC_RSim_Linear, 'Enable', 'on')
set(handles.txtNoP_SimRRT_Linear, 'Enable', 'off')
set(handles.pbUpNoP_SimRRT_Linear, 'Enable', 'off')
set(handles.pbDownNoP_SimRRT_Linear, 'Enable', 'off')
set(handles.rbYesEPS_SimRRT_Linear, 'Enable', 'off')
set(handles.rbNoEPS_SimRRT_Linear, 'Enable', 'off')
set(handles.txtSF_SimRRT_Linear, 'Enable', 'off')


function txtNoP_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtNoP_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNoP_RSim_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtNoP_RSim_Linear as a double
value = str2double(get(hObject, 'String'));

if value < 1
    value = 1;
end

value = num2str(value);
set(hObject, 'String', value);


% --- Executes during object creation, after setting all properties.
function txtNoP_RSim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNoP_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbDownNoP_RSim_Linear.
function pbDownNoP_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbDownNoP_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtNoP_RSim_Linear, 'String');
value = str2double(value) - 1;

if value > 0
    value = num2str(value);
    set(handles.txtNoP_RSim_Linear, 'String', value);
end


% --- Executes on button press in pbUpNoP_RSim_Linear.
function pbUpNoP_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbUpNoP_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtNoP_RSim_Linear, 'String');
value = str2double(value) + 1;
value = num2str(value);
set(handles.txtNoP_RSim_Linear, 'String', value);


function txtFV_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtFV_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFV_RSim_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtFV_RSim_Linear as a double
value = str2double(get(hObject, 'String'));
if value > 1
    value = 1;
elseif value < 0
    value = 0;
end

value = num2str(value);
set(hObject, 'String', value);

% --- Executes during object creation, after setting all properties.
function txtFV_RSim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFV_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.



function txtFIV_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtFIV_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFIV_RSim_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtFIV_RSim_Linear as a double
value = str2double(get(hObject, 'String'));
if value > 1
    value = 1;
elseif value < 0
    value = 0;
end

value = num2str(value);
set(hObject, 'String', value);


% --- Executes during object creation, after setting all properties.
function txtFIV_RSim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFIV_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtIC_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtIC_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtIC_RSim_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtIC_RSim_Linear as a double
value = str2double(get(hObject, 'String'));

if value < 1
    value = 1;
end

value = num2str(value);
set(hObject, 'String', value);

% --- Executes during object creation, after setting all properties.
function txtIC_RSim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtIC_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in rbSimRRT_Linear.
function rbSimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbSimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbSimRRT_Linear

set(hObject, 'Enable', 'off')
set(handles.rbRSim_Linear, 'Enable', 'on')
set(handles.rbRSim_Linear, 'Value', 0)
set(handles.rbSim_Linear, 'Enable', 'on')
set(handles.rbSim_Linear, 'Value', 0)

set(handles.txtX0_Sim_Linear, 'Enable', 'off')
set(handles.txtU_Sim_Linear, 'Enable', 'off')
set(handles.popX0_Sim_Linear, 'Enable', 'off')
set(handles.popU_Sim_Linear, 'Enable', 'off')
set(handles.txtNoP_RSim_Linear, 'Enable', 'off')
set(handles.pbUpNoP_RSim_Linear, 'Enable', 'off')
set(handles.pbDownNoP_RSim_Linear, 'Enable', 'off')
set(handles.txtFV_RSim_Linear, 'Enable', 'off')
set(handles.txtFIV_RSim_Linear, 'Enable', 'off')
set(handles.txtIC_RSim_Linear, 'Enable', 'off')
set(handles.pbUpIC_RSim_Linear, 'Enable', 'off')
set(handles.pbDownIC_RSim_Linear, 'Enable', 'off')
set(handles.txtNoP_SimRRT_Linear, 'Enable', 'on')
set(handles.pbUpNoP_SimRRT_Linear, 'Enable', 'on')
set(handles.pbDownNoP_SimRRT_Linear, 'Enable', 'on')
set(handles.rbNoEPS_SimRRT_Linear, 'Enable', 'on')
set(handles.txtSF_SimRRT_Linear, 'Enable', 'on')


function txtNoP_SimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtNoP_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNoP_SimRRT_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtNoP_SimRRT_Linear as a double
value = str2double(get(hObject, 'String'));

if value < 1
    value = 1;
end

value = num2str(value);
set(hObject, 'String', value);

% --- Executes during object creation, after setting all properties.
function txtNoP_SimRRT_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNoP_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbDownNoP_SimRRT_Linear.
function pbDownNoP_SimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbDownNoP_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtNoP_SimRRT_Linear, 'String');
value = str2double(value) - 1;

if value > 0
    value = num2str(value);
    set(handles.txtNoP_SimRRT_Linear, 'String', value);
end

% --- Executes on button press in pbUpNoP_SimRRT_Linear.
function pbUpNoP_SimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbUpNoP_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtNoP_SimRRT_Linear, 'String');
value = str2double(value) + 1;
value = num2str(value);
set(handles.txtNoP_SimRRT_Linear, 'String', value);


% --- Executes on button press in pbDownIC_RSim_Linear.
function pbDownIC_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbDownIC_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtIC_RSim_Linear, 'String');
value = str2double(value) - 1;

if value > 0
    value = num2str(value);
    set(handles.txtIC_RSim_Linear, 'String', value);
end

% --- Executes on button press in pbUpIC_RSim_Linear.
function pbUpIC_RSim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to pbUpIC_RSim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = get(handles.txtIC_RSim_Linear, 'String');
value = str2double(value) + 1;
value = num2str(value);
set(handles.txtIC_RSim_Linear, 'String', value);


% --- Executes on button press in rbYesEPS_SimRRT_Linear.
function rbYesEPS_SimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbYesEPS_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbYesEPS_SimRRT_Linear
set(hObject, 'Enable', 'off')
set(handles.rbNoEPS_SimRRT_Linear, 'Enable', 'on')
set(handles.rbNoEPS_SimRRT_Linear, 'Value', 0)


% --- Executes on button press in rbNoEPS_SimRRT_Linear.
function rbNoEPS_SimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbNoEPS_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbNoEPS_SimRRT_Linear
set(hObject, 'Enable', 'off')
set(handles.rbYesEPS_SimRRT_Linear, 'Enable', 'on')
set(handles.rbYesEPS_SimRRT_Linear, 'Value', 0)


function txtSF_SimRRT_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtSF_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtSF_SimRRT_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtSF_SimRRT_Linear as a double

value = str2double(get(hObject, 'String'));
if value < 1
    value = 1;
end

value = num2str(value);
set(hObject, 'String', value);


% --- Executes during object creation, after setting all properties.
function txtSF_SimRRT_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtSF_SimRRT_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popX0_Sim_Linear.
function popX0_Sim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to popX0_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popX0_Sim_Linear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popX0_Sim_Linear
contents = cellstr(get(hObject,'String'));
x0 = contents{get(hObject,'Value')};
set(handles.txtX0_Sim_Linear,'String', x0);
handles.x0 = x0;
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popX0_Sim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popX0_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popU_Sim_Linear.
function popU_Sim_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to popU_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popU_Sim_Linear contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popU_Sim_Linear
contents = cellstr(get(hObject,'String'));
u = contents{get(hObject,'Value')};
set(handles.txtU_Sim_Linear,'String', u);
handles.u = u;
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function popU_Sim_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popU_Sim_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtUTrans_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtUTrans_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtUTrans_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtUTrans_Linear as a double

WS_vars = evalin('base', 'who');
WS_vars{end+1} = '';

uTrans = get(hObject, 'String');
handles.uTrans = uTrans;
guidata(hObject, handles);
set(handles.popUTrans_Linear, 'Value', length(WS_vars))



% --- Executes during object creation, after setting all properties.
function txtUTrans_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtUTrans_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function txtUTransVec_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to txtUTransVec_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtUTransVec_Linear as text
%        str2double(get(hObject,'String')) returns contents of txtUTransVec_Linear as a double
WS_vars = evalin('base', 'who');
WS_vars{end+1} = '';

uTransVec = get(hObject, 'String');
handles.uTransVec = uTransVec;
guidata(hObject, handles);
set(handles.popUTransVec_Linear, 'Value', length(WS_vars))


% --- Executes during object creation, after setting all properties.
function txtUTransVec_Linear_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtUTransVec_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in rbOriginYes_Linear.
function rbOriginYes_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbOriginYes_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbOriginYes_Linear
set(hObject, 'Enable', 'off')
set(handles.rbOriginNo_Linear, 'Enable', 'on')
set(handles.rbOriginNo_Linear, 'Value', 0)

% --- Executes on button press in rbOriginNo_Linear.
function rbOriginNo_Linear_Callback(hObject, eventdata, handles)
% hObject    handle to rbOriginNo_Linear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbOriginNo_Linear
set(hObject, 'Enable', 'off')
set(handles.rbOriginYes_Linear, 'Enable', 'on')
set(handles.rbOriginYes_Linear, 'Value', 0)
