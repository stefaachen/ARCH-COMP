function Zenclose = enclosingZonotope(Z,V)
% polytope - Encloses a zonotope by a parallelotope
%
% Syntax:  
%    Zenclose = enclosingZonotope(Z,V)
%
% Inputs:
%    Z - zonotope object
%    V - vertices
%
% Outputs:
%    Zenclose - enclosing zonotope object
%
% Example: 
%    Z = zonotope([0;0],rand(2,5));
%    V = {[1;0]};
%    Zenclose = enclosingZonotope(Z,V);
% 
%    plot(Z); hold on;
%    plot(Zenclose,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval,  vertices

% Author:       Matthias Althoff
% Written:      02-October-2008
% Last update:  25-July-2016 (intervalhull replaced by interval)
% Last revision:---

%------------- BEGIN CODE --------------

%delete empty vertices cells
i=1;
while i<=length(V)
    if isempty(V{i})
        V(i)=[];
    else
        i=i+1;
    end
end

if ~isempty(V)

    Min1=min(V{1},[],2);
    Max1=max(V{1},[],2);
    Min2=min(V{length(V)},[],2);
    Max2=max(V{length(V)},[],2);
    IH1=interval(Min1,Max1);
    IH2=interval(Min2,Max2);

    c1=center(IH1);
    c2=center(IH2);
    
    addGen=zonotope([0*c1,c2-c1]);
    
    Z=Z+addGen;
    
    newP=reduce(Z,'methFdP');

    Vsum=[];
    for i=1:length(V)
    	Vsum=[Vsum,V{i}];
    end
    
  
    %compute oriented rectangular hull
    Vtrans=inv(newP)*Vsum;
    Min=min(Vtrans,[],2);
    Max=max(Vtrans,[],2);
    IHtrans=interval(Min,Max);
    Ztrans=zonotope(IHtrans);
    Zenclose=newP*Ztrans;

    
else
    Zenclose=[];
end


    
%------------- END OF CODE --------------