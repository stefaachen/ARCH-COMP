function createHessianTensorFile(J2dyn,J2con,path,name,vars,infsupFlag,options)
% createHessianTensorFile - generates an mFile that allows to compute the
% hessian tensor
%
% Syntax:  
%    createHessianTensorFile(obj,path)
%
% Inputs:
%    obj - nonlinear system object
%    path - path for saving the file
%    name - name of the nonlinear function to which the hessian belongs
%
% Outputs:
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      21-August-2012
% Last update:  08-March-2017
%               05-November-2017
%               03-December-2017
%               13-March-2020 (NK, implemented options.simplify = optimize)
% Last revision:---

%------------- BEGIN CODE --------------

% ckeck if the taylor models or zoo-objects are used to evaluate the
% remainder
taylMod = 0;

if isfield(options,'lagrangeRem') && isfield(options.lagrangeRem,'method') && ...
   ~strcmp(options.lagrangeRem.method,'interval')
    if ~ismember(options.lagrangeRem.method,{'taylorModel','zoo'})
       error('Wrong value for setting "options.lagrangeRem.method"!');
    end
    taylMod = 1;
end

% check if tensor evaluation is optimized for lower computation time
if isfield(options,'simplify') && strcmp(options.simplify,'optimize')
   opt = 1; 
else
   opt = 0; 
end

% init
Hdyn = cell(length(vars.x),1);
Hcon = cell(length(vars.y),1);

% squeeze dynamic part
for k=1:length(vars.x)
    Hdyn{k} = squeeze(J2dyn(k,:,:));
end
% squeeze constraint part
for k=1:length(vars.y)
    Hcon{k} = squeeze(J2con(k,:,:));
end

fid = fopen([path '/hessianTensor_',name,'.m'],'w');
% function arguments depending on occurring variable types
if isempty(vars.p)
    if isempty(vars.y) % no constraints
        if isempty(vars.T)
            fprintf(fid, '%s\n\n', ['function Hf=hessianTensor_',name,'(x,u)']);
            args = '(x,u)';
            symVars = 'vars.x,vars.u';
        else
            fprintf(fid, '%s\n\n', ['function Hf=hessianTensor_',name,'(x,u,T)']);
            args = '(x,u,T)';
            symVars = 'vars.x,vars.u,vars.T';
        end
    else % with constraints
        if isempty(vars.T)
            fprintf(fid, '%s\n\n', ['function [Hf,Hg]=hessianTensor_',name,'(x,y,u)']);
            args = '(x,y,u)';
            symVars = 'vars.x,vars.y,vars.u';
        else
            fprintf(fid, '%s\n\n', ['function [Hf,Hg]=hessianTensor_',name,'(x,y,u,T)']);
            args = '(x,y,u,T)';
            symVars = 'vars.x,vars.y,vars.u,vars.T';
        end
    end
else
    if isempty(vars.T)
        fprintf(fid, '%s\n\n', ['function Hf=hessianTensor_',name,'(x,u,p)']);
        args = '(x,u,p)';
        symVars = 'vars.x,vars.u,vars.p';
    else
        fprintf(fid, '%s\n\n', ['function Hf=hessianTensor_',name,'(x,u,p,T)']);
        args = '(x,u,p,T)';
        symVars = 'vars.x,vars.u,vars.p,vars.T';
    end
end

% write call to optimized function to reduce the number of interval operations
if opt  
    % write function
    str = ['out = funOptimize',args,';'];
    fprintf(fid, '\n\n %s\n\n', str);
    
    % store indices of nonempty enries
    counter = 1;
    ind = cell(size(Hdyn));
    out = [];
    
    for i = 1:length(Hdyn)
       [r,c] = find(Hdyn{i});
       if ~isempty(r)
           ind{i}.row = r;
           ind{i}.col = c;
           ind{i}.index = counter:counter + length(r)-1;
           counter = counter + length(r);
           for j = 1:length(r)
              out = [out;Hdyn{i}(r(j),c(j))]; 
           end
       end
    end  
end

%dynamic part
for k=1:length(Hdyn)
    %get matrix size
    [rows,cols] = size(Hdyn{k});
    sparseStr = ['sparse(',num2str(rows),',',num2str(cols),')'];
    if infsupFlag 
        str=['Hf{',num2str(k),'} = interval(',sparseStr,',',sparseStr,');'];
    else
        str=['Hf{',num2str(k),'} = ',sparseStr,';'];
    end
    %write in file if Hessian is used as Lagrange remainder
    fprintf(fid, '\n\n %s\n\n', str);
    % write rest of matrix
    if ~opt
        if infsupFlag && taylMod
            writeSparseMatrixTaylorModel(Hdyn{k},['Hf{',num2str(k),'}'],fid);
        else
            writeSparseMatrix(Hdyn{k},['Hf{',num2str(k),'}'],fid);
        end
    elseif ~isempty(ind{k})
        writeSparseMatrixOptimized(ind{k},['Hf{',num2str(k),'}'],fid,taylMod);
    end
    
    disp(['dynamic dim ',num2str(k)]);
end

%constraint part
for k=1:length(Hcon)
    %get matrix size
    [rows,cols] = size(Hcon{k});
    sparseStr = ['sparse(',num2str(rows),',',num2str(cols),')'];
    if infsupFlag 
        str=['Hg{',num2str(k),'} = interval(',sparseStr,',',sparseStr,');'];
    else
        str=['Hg{',num2str(k),'} = ',sparseStr,';'];
    end
    %write in file if Hessian is used as Lagrange remainder
    fprintf(fid, '\n\n %s\n\n', str);
    % write rest of matrix
    writeSparseMatrix(Hcon{k},['Hg{',num2str(k),'}'],fid);
    
    disp(['dynamic dim ',num2str(k)]);
end

% create optimized function to reduce the number of interval operations
if opt
    % create file with optimized evaluation
    pathTemp = fullfile(path,'funOptimize.m');
    str = ['matlabFunction(out,''File'',pathTemp,''Vars'',{',symVars,'});'];
    eval(str);
    
    % read in text from the file
    text = fileread(pathTemp);

    % print text from file
    fprintf(fid, '%s',text);
end

%close file
fclose(fid);



%------------- END OF CODE --------------