function [zonotopes]=segmentZonotope(Obj,varargin)
% Purpose:  return zonotope of desired segment
% Pre:      partition object
% Post:     polytope object
% Built:    29.09.06,MA
% Modified: 1.8.17 AP

%generate zonotope out of cell--------------------------
if nargin > 1
    [intervals]=segmentInterval(Obj,varargin{1});
else
    [intervals]=segmentInterval(Obj);
end


for i = 1:length(intervals)
    IH=intervals{i};
    zonotopes{i} = zonotope(IH);
end
%-------------------------------------------------------- 
