#!/bin/bash

OUTFILE="tests-light.out"

export LD_LIBRARY_PATH=lib

echo "Running tests using SpaceEx + PPLite" > $OUTFILE
SPACEEX='./bin/memtime ./bin/sspaceex'

function run {
    echo "Benchmark" $2 ":"
    $SPACEEX -g $1/$2.cfg -m $1/$2.xml -vl
    echo "=============================================================="
}

function run_tests {
    # Adaptive Cruise Controller
    for name in ACCS05-UBD05 ACCU05-UBD05 ACCS06-UBD06 ACCU06-UBD06 ; do
        run ACC $name
    done

    # Distributed Controller
    for name in DISC02-UBS02 ; do
        run DISC $name
    done

    # Dutch Railway Network
    for name in DRNW02-BDR01 ; do
        run DRNW $name
    done

    # Fischer
    for name in FISCS04-UBD04 FISCU04-UBD04 ; do
        run FISC $name
    done

    # TTEthernet
    for name in TTES05-UBD05 TTES07-UBD07 ; do
        run TTE $name
    done
}

run_tests >> $OUTFILE 2>&1

PATTERN='Benchmark\|Found fixpoint\|Forbidden\|Computing reachable states done after\|Max VSize'
grep "$PATTERN" $OUTFILE > $OUTFILE.filtered
