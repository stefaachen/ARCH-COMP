function res = test_partition_get_polytopes()
% test of the function segmentPolytope

%setup partitions
threeDimField=partition([0,10; -3,3; 0,1],[5;10;3]);
twoDimField=partition([0,10; -3,3],[5;10]);

% check that segmentPolytope works, 3DOF
Pols1 = segmentPolytope(twoDimField,0:twoDimField.totalSegments);
Pols2 = segmentPolytope(twoDimField);

if length(Pols1)==length(Pols2)
    res1 = (norm(volume(Pols1{3}) - volume(Pols2{3}))<1e-9);
else
    res1 = 0;
end


% check that segmentPolytope works, 3DOF
Pols1 = segmentPolytope(threeDimField,0:threeDimField.totalSegments);
Pols2 = segmentPolytope(threeDimField);

if length(Pols1)==length(Pols2)
    res2 = (norm(volume(Pols1{3}) - volume(Pols2{3}))<1e-9);
else
    res2 = 0;
end


res = res1&&res2;
% 
% segmentPolytope(threeDimField,[1 5 3])
% segmentPolytope(threeDimField)
% segmentPolytope(threeDimField,[1 5 3])
% segmentPolytope(threeDimField)
% P = mptPolytope([2 0 0.3;4 2 0.6;1 1 0.5; 1 1 0.1]);
% intersectingSegments(threeDimField,P)
% [iS,percentages] = exactIntersectingSegments(threeDimField,P)
% plot(threeDimField,exactIntersectingSegments(threeDimField,P))
% hold on
% plot(P)
% 
% %partition with 