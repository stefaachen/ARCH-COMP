#!/usr/bin/env bash

mkdir -p output

echo "Run building 20 sec model."
./bin/hydra-cli -m benchmarks/building_more_decimals_20sec.model -r support_function
echo "\n\ncleanup and processing plots"
mkdir -p output/building_more_decimals_20sec
mv *.plt output/building_more_decimals_20sec/
mv *.gen output/building_more_decimals_20sec/
mv *.gv output/building_more_decimals_20sec/
pushd output/building_more_decimals_20sec
	gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "\n\nRun building 1 sec model."
./bin/hydra-cli -m benchmarks/building_more_decimals_1sec.model -r support_function
echo "\n\ncleanup and processing plots"
mkdir -p output/building_more_decimals_1sec
mv *.plt output/building_more_decimals_1sec/
mv *.gen output/building_more_decimals_1sec/
mv *.gv output/building_more_decimals_1sec/
pushd output/building_more_decimals_1sec
	gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "\n\nRun platoon BND42 model."
./bin/hydra-cli -m benchmarks/platoon_DBND42.model -s tacas18_6
echo "\n\ncleanup and processing plots"
mkdir -p output/platoon_DBND42
mv *.plt output/platoon_DBND42/
mv *.gen output/platoon_DBND42/
mv *.gv output/platoon_DBND42/
pushd output/platoon_DBND42
	gnuplot *.plt
popd
echo "done."
