ArchiveEntry "Wien bridge oscillator".

ProgramVariables.
 R x.   /* x-coordinate */
 R y.   /* y-coordinate */
End.

Problem.
x^2 <= 1/2 & y^2 <= 1/3 ->
 [
   {x'=-x - (1117*y)/500 + (439*y^3)/200 - (333*y^5)/500, y'=x + (617*y)/500 - (439*y^3)/200 + (333*y^5)/500}
 ] (x - 4*y < 8)
End.

Tactic "Scripted proof".
  /* invariant not generated by Pegasus */
  implyR(1);
  dC({`x^2 + x*y + y^2 - 111/59 <= 0`}, 1); <(
    dW(1); QE; done
    ,
    ODE(1); done
  )
End.

End.

ArchiveEntry "Locally stable nonlinear system".

ProgramVariables.
 R x.   /* x-coordinate */
 R y.   /* y-coordinate */
End.

Problem.
  x^2 <= 1/2 & (y + 2)^2 <= 1/3 -> [{x'=-x + y - x^3, y'=-x - y + y^2}] ((-1 + x)^2 + (-(3/2) + y)^2 > 1/4)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  implyR(1);
  dC({`2*x^2 + (y + 3/2)^2 - 4 <= 0`}, 1); <(
    dW(1); QE; done
    ,
    ODE(1); done
  )
End.

End.

ArchiveEntry "MIT astronautics Lyapunov".

ProgramVariables.
 R x.   /* x-coordinate */
 R y.   /* y-coordinate */
End.

Problem.
  x^2 <= 1/2 & y^2 <= 1/3 ->
    [{x'=y - x^7*(x^4 + 2*y^2 - 10), y'=-x^3 - 3*(y^5)*(x^4 + 2*y^2 - 10)}] ((-2 + x)^2 + (-3 + y)^2 > 1/4)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  implyR(1);
  dC({`x^4 + 2*y^2 - 19 <= 0`}, 1); <(
    dW(1); QE; done
    ,
    ODE(1); done
  )
End.

End.

ArchiveEntry "1D Saddle Node".

ProgramVariables.
  R x.
  R r.
  R s.
End.

Problem.
  r <= 0 -> \exists f (x=f -> [{x'=r+x^2}]x=f)
End.

Tactic "Scripted proof".
implyR(1); cutR({`\exists sr sr^2=-r`},1); <(
  QE
  ,
  implyR(1);
  existsL('Llast);
  existsR({`sr`}, 1);
  implyR(1);
  dC({`x-sr=0`},1); <(
    dW(1); QE,
    dbx({`x+sr`},1)
  )
)
End.

End.

ArchiveEntry "Ahmadi Parrilo Krstic".

Description "Theorem 1.1 (1)".
Citation "Amir Ali Ahmadi, Miroslav Krstic, and Pablo A. Parrilo. A globally asymptotically stable polynomial vector field with no polynomial Lyapunov function. CDC-ECE 2011".
Link "https://doi.org/10.1109/CDC.2011.6161499".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  0.5<=x & x<=0.7 & 0<=y & y<=0.3 ->
  [{x'=-x+x*y , y'=-y}@invariant(y>=0)]
  !(-0.8>=x & x>=-1 & -0.7>=y & y>=-1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Alongi Nelson Ex_4_1_9 page 143".

Description "Ex 4.1.9 Page 143".
Citation "John M. Alongi and Gail S. Nelson. Recurrence and Topology".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x = 1 & y = 0 & z = 0 ->
  [{x'=x*z , y'=y*z , z'=-x^2-y^2 & x^2+y^2+z^2=1}@invariant(z<=0)]
  !(x > 1 | z > 0)
End.

End.

ArchiveEntry "Arrowsmith Place Fig_1_29 page 14".

Description "Fig 1.29 Page 14".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=0 & y=-1 -> [{x'=x, y'=y^2}@invariant(y<=0)]!(y>0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_1_30 page 14".

Description "Fig 1.30 Page 14".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1 & y=0 -> [{x'=y^2, y'=x}@invariant(-1*x<=0)]!(x<0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_1_31 page 14".

Description "Fig 1.31 Page 14".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=-1 & y=1 -> [{x'=x^2, y'=(2*x-y)*y}@invariant((-1*y < 0&x<=0), (-1*y < 0), (x<=0))]!(x>0 | y<=0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_1_32 page 14".

Description "Fig 1.32 Page 14".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x = -1 & y = 1 ->
  [{x'=-(x*y) , y'=x^2+y^2}@invariant(-1*y < 0&x<=0)]
  !(x > 0 | y <= 0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_1_35 page 17".

Description "Fig 1.35 Page 17".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1 & y=1 -> [{x'=x*(2-x-2*y), y'=(2-2*x-y)*y}@invariant((-1*x < 0&-1*y < 0), (-1*x < 0), (-1*y < 0))]!(x<=0 | y<=0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_3_1 page 72".

Description "Fig 3.1 Page 72".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2<=1/5 -> [{x'=-4*y+x*(1-x^2-y^2)-y*(x^2+y^2), y'=4*x+y*(1-x^2-y^2)+x*(x^2+y^2)}@invariant(-1+x^2+y^2<=0)]!(x^2+y^2>1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_3_5c page 79".

Description "Fig 3.5c Page 79".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2>=1 -> [{x'=x-y^3, y'=x^3+y}@invariant(1/2+-1*x^2+-1*y^2<=0)]!(x^2+y^2<1/2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_3_5e page 79".

Description "Fig 3.5e Page 79".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1 & y=-1 -> [{x'=x^2+(x+y)/2, y'=(-x+3*y)/2}]!(y>0)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`y - x + 1 <=0 & y<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Arrowsmith Place Fig_3_8 page 82".

Description "Fig 3.8 Page 82".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1 & y=-1 -> [{x'=x*(x+2*y), y'=y*(2*x+y)}@invariant((-1*x<=0&y<=0), (-1*x<=0), (y<=0))]!(x<0 | y>0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_3_9 page 83".

Description "Fig 3.9 Page 83".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=-1 & y=-1 -> [{x'=x*(x-2*y), y'=-(2*x-y)*y}@invariant((x<=0&y<=0), (x<=0), (y<=0))]!(x>0 | y>0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Arrowsmith Place Fig_3_11 page 83".

Description "Fig 3.11 Page 83".
Citation "D. K. Arrowsmith and C. M. Place. An Introduction to Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x=1 & y=1/8 -> [{x'=x-y^2, y'=y*(x-y^2)}@invariant(x+-1*y^2>=0)]!(x<0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Ben Sassi Girard 20104 Moore-Greitzer Jet".

Description "6.1 Moore-Greitzer jet engine model".
Citation "Mohamed Amin Ben Sassi, Antoine Girard. Controller synthesis for robust invariance of polynomial dynamical systems using linear programming".
Link "https://doi.org/10.1016/j.sysconle.2012.01.004".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  -1/5000 + (1/20+x)^2 + (3/200 + y)^2 <= 0
  ->
  [{x'=(-3*x^2)/2-x^3/2-y, y'=3*x-y}]!(49/100 + x + x^2 + y + y^2 <= 0)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.073036*x^6-0.014461*x^5*y+0.059693*x^4*y^2-0.0063143*x^3*y^3+0.029392*x^2*y^4+0.0036316*y^6+0.064262*x^5+0.24065*x^4*y-0.082711*x^3*y^2+0.28107*x^2*y^3-0.015542*x*y^4+0.036437*y^5+0.47415*x^4-0.56542*x^3*y+1.1849*x^2*y^2-0.22203*x*y^3+0.19053*y^4-0.59897*x^3+1.8838*x^2*y-0.59653*x*y^2+0.47413*y^3+1.0534*x^2-0.51581*x*y+0.43393*y^2-0.35572*x-0.11888*y-0.25586<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Ben Sassi Girard Sankaranarayanan 2014 Fitzhugh-Nagumo".

Description "Fitzhugh-Nagumo system".
Citation "Mohamed Amin Ben Sassi, Antoine Girard, Sriram Sankaranarayanan. Iterative computation of polyhedral invariants sets for polynomial dynamical systems".
Link "10.1109/CDC.2014.7040384".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  -1/20 + (5/4+x)^2 + (-5/4+y)^2 <= 0
  ->
  [{x'=7/8+x-x^3/3-y, y'=(2*(7/10+x-(4*y)/5))/25}]!(36/5 + 5*x + x^2 + 2*y + y^2 <= 0)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.12152*x^4+0.22807*x^3*y+0.214*x^2*y^2-0.71222*y^4-0.27942*x^3-0.48799*x^2*y-0.2517*x*y^2-0.3366*y^3-0.21526*x^2+0.16728*x*y-0.44613*y^2+0.35541*x-0.21594*y-0.72852<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Bhatia Szego Ex_2_4 page 68".

Description "Ex 2.4 Page 68".
Citation "Bhatia, N. P. and Szego, G. P.. Stability Theory of Dynamical Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + (-1/2 + y)^2 < 1/24 ->
  [{x'=-x+2*x^3*y^2 , y'=-y & x^2*y^2<1}]
  !(x <= -2 | y <= -1)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-0.00028346*x^6*y^3 - 0.00049999*x^5*y^4 + 0.00018784*x^4*y^5 + 0.0002836*x^3*y^6 - 0.00020211*x^2*y^7 - 0.00011251*x*y^8 + 0.00032872 *y^9 - 0.00032162*x^7*y - 0.0012954*x^6*y^2 - 0.0038775*x^5*y^3 - 0.0085289*x^4*y^4 + 0.0016293*x^3*y^5 + 0.0018949*x^2*y^6 - 0.0035577 *x*y^7 + 0.011859*y^8 - 0.00043993*x^7 - 0.0050214*x^6*y - 0.0047617*x^5 *y^2 + 0.011009*x^4*y^3 + 0.012178*x^3*y^4 + 0.0055785*x^2*y^5 + 0.0037099*x*y^6 - 0.062198*y^7 - 0.0023971*x^6 + 0.005244*x^5*y + 0.0031699*x^4*y^2 + 0.00069299*x^3*y^3 + 0.14159*x^2*y^4 - 0.081176*x *y^5 + 0.27277*y^6 + 0.0089006*x^5 + 0.10703*x^4*y + 0.0021026*x^3*y^2 - 0.39217*x^2*y^3 + 0.17202*x*y^4 - 0.85217*y^5 + 0.052331*x^4 - 0.008468*x^3*y - 0.21988*x^2*y^2 - 0.19426*x*y^3 + 1.3672*y^4 - 0.067608*x^3 - 1.3917*x^2*y - 1.3541*x*y^2 - 1.0593*y^3 - 0.80894*x^2 - 2.9459*x*y + 1.0078*y^2 - 1.8819*x - 2.2726*y - 1.433 <= 0 & y>=0 & x>=-2`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Collin Goriely page 60".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (2+x)^2 + (-1+y)^2 <= 1/4
  ->
  [{x'=x^2+2*x*y+3*y^2, y'=2*y*(2*x+y)}]!(x>0)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`y>=0 & x+y<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Dai Gan Xia Zhan JSC14 Ex. 1".

Description "Example 1".
Citation "Liyun Dai, Ting Gan, Bican Xia, Naijun Zhan. Barrier certificates revisited. J. Symb. Comput.".
Link "https://doi.org/10.1016/j.jsc.2016.07.010".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  1/100 - x^2 - y^2 >= 0 -> [{x'=-2*x+x^2+y, y'=x-2*y+y^2}@invariant(-1/4+x^2+y^2 < 0)]!(x^2+y^2 >= 1/4)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dai Gan Xia Zhan JSC14 Ex. 2".

Description "Example 2".
Citation "Liyun Dai, Ting Gan, Bican Xia, Naijun Zhan. Barrier certificates revisited. J. Symb. Comput.".
Link "https://doi.org/10.1016/j.jsc.2016.07.010".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + (2+y)^2 <= 1
  ->
  [{x'=2*x-x*y, y'=2*x^2-y}]!(x^2+(-1+y)^2 <= 9/100)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.0052726*x^10+0.013182*x^8*y^2+0.013181*x^6*y^4+0.0065909*x^4*y^6+0.0016477*x^2*y^8+0.00016477*y^10-0.060426*x^8*y-0.11666*x^6*y^3-0.08401*x^4*y^5-0.02829*x^2*y^7-0.0026618*y^9-0.0093935*x^8+0.25715*x^6*y^2+0.35556*x^4*y^4+0.18385*x^2*y^6+0.017843*y^8-0.22922*x^6*y-0.82409*x^4*y^3-0.6654*x^2*y^5-0.072582*y^7+0.38533*x^6+1.6909*x^4*y^2+1.7759*x^2*y^4+0.20099*y^6+1.8855*x^4*y-0.83113*x^2*y^3-0.10854*y^5-4.9159*x^4-11.581*x^2*y^2-1.9047*y^4+6.644*x^2*y+7.8358*y^3+1.5029*x^2-13.2338*y^2+10.8962*y-3.4708<=0&0.10731*x^10+0.26827*x^8*y^2+0.26827*x^6*y^4+0.13413*x^4*y^6+0.033534*x^2*y^8+0.0033532*y^10-1.2677*x^8*y-2.4914*x^6*y^3-1.8208*x^4*y^5-0.59588*x^2*y^7-0.057773*y^9-0.82207*x^8+4.1107*x^6*y^2+6.7924*x^4*y^4+3.4828*x^2*y^6+0.36938*y^8+6.8306*x^6*y-0.93431*x^4*y^3-5.9328*x^2*y^5-0.95223*y^7+2.2556*x^6-17.4284*x^4*y^2-6.4448*x^2*y^4-0.33741*y^6-1.2936*x^4*y+16.8675*x^2*y^3+8.8828*y^5-16.1915*x^4-39.7751*x^2*y^2-25.8126*y^4+43.7284*x^2*y+39.2116*y^3-12.7866*x^2-33.0675*y^2+15.2878*y-3.1397<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Dai Gan Xia Zhan JSC14 Ex. 5".

Description "Example 5".
Citation "Liyun Dai, Ting Gan, Bican Xia, Naijun Zhan. Barrier certificates revisited. J. Symb. Comput.".
Link "https://doi.org/10.1016/j.jsc.2016.07.010".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (1+x)^2 + (-2+y)^2 <= 4/25
  ->
  [{x'=y, y'=2*x-x^3-y-x^2*y}]!((-1+x)^2 + y^2 <= 1/25)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.23942*x^6 + 0.097208*x^5*y + 0.06013*x^4*y^2 - 0.0076888*x^3*y^3
  - 0.022097*x^2*y^4 + 0.067444*x*y^5 + 0.063249*y^6 - 0.11511*x^5
  - 0.093461*x^4*y - 0.061763*x^3*y^2 + 0.065902*x^2*y^3 + 0.053766*x*y^4
  - 0.1151*y^5 - 0.95442*x^4 + 0.38703*x^3*y + 0.46309*x^2*y^2 - 0.14691*x
  *y^3 + 0.11756*y^4 - 0.021196*x^3 - 0.40047*x^2*y - 0.28433*x*y^2
  - 0.028468*y^3 - 0.020192*x^2 - 0.37629*x*y - 0.13713*y^2 + 1.9803*x
  - 1.4121*y - 0.51895<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Darboux Christoffel Int Goriely page 58".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2+y^2<=1 -> [{x'=3*(-4+x^2), y'=3+x*y-y^2}]!(x < -4 | y < -4 | x>4 | y>4)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-0.0013138*x^5 + 0.00048141*x^4*y + 0.000828*x^2*y^3 - 0.0016748*x*y^4
  + 0.0008106*y^5 + 0.010722*x^4 - 0.0018729*x^3*y + 0.0041383*x^2*y^2
  - 0.013911*x*y^3 + 0.0085091*y^4 - 0.039948*x^3 - 0.0060006*x^2*y
  - 0.046355*x*y^2 + 0.054433*y^3 - 0.028132*x^2 + 0.13217*x*y + 0.10916
  *y^2 + 0.62004*x - 0.88775*y - 1.1161<=0 &  -0.00011438*x^4*y^2 + 0.00015105*x^2*y^4 - 0.0018063*x^5 + 0.0012699*x^3
  *y^2 + 0.0014498*x^2*y^3 - 0.0014334*x*y^4 + 0.0013001*y^5 + 0.017567
  *x^4 + 0.0050023*x^3*y - 0.0016674*x^2*y^2 - 0.015315*x*y^3 + 0.01038
  *y^4 - 0.072259*x^3 - 0.035874*x^2*y - 0.050558*x*y^2 + 0.058708*y^3
  - 0.05097*x^2 + 0.042626*x*y + 0.19257*y^2 + 1.3148*x + 0.014613*y
  - 1.2585 <= 0 & x^2 + y^2 - 16 <= 0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Djaballah Chapoutot Kieffer Bouissou 2015 Ex. 1".

Description "Example 1".
Citation "Adel Djaballah, Alexandre Chapoutot, Michel Kieffer, Olivier Bouissou. Construction of parametric barrier functions for dynamical systems using interval analysis. Automatica 78".
Link "https://doi.org/10.1016/j.automatica.2016.12.013".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  -1/20 + (5/4 + x)^2 + (-5/4 + y)^2 <= 0 ->
  [{x'=x+y , y'=x*y-y^2/2}]
  !((5/2 + x)^2 + (-4/5 + y)^2 <= 1/20)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-0.10316*x^4 - 0.44099*x^3 + 0.7506*x^2*y - 0.3745*x*y^2 + 0.21087*y^3 - 0.35638*x^2 - 0.16318*x*y - 0.87072*y^2 + 0.44863*x - 0.6858*y - 1.4514 <= 0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 1_9a".

Description "Example 1.9a".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x>-1/2 & x < -1/3 & y<0 & y>=-1/2
  ->
  [{x'=x*(1-x^2-y^2)+y*((-1+x^2)^2+y^2), y'=y*(1-x^2-y^2)-y*((-1+x^2)^2+y^2)}]!(x>=0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 1_9b".

Description "Example 1.9b".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a>-1/2 & a < -1/3 & y<0 & y>=-1/2 ->
  [{a'=a*(1-a^2-y^2)+y*((-1+a^2)^2+y^2), y'=y*(1-a^2-y^2)-y*((-1+a^2)^2+y^2)}
   @invariant(-2+a^2+2*y^2<=0)
  ]!(a^2+2*y^2 > 2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 1_11a".

Description "Exercise 1.11".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -1/2 & x < -1/3 & y < 0 & y >= -1/2 -> [{x'=2*x-x^5-x*y^4, y'=y-x^2*y-y^3}@invariant((x<=0&y<=0), (x<=0), (y<=0))]!(x + y > 0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 10_11b".

Description "Exercise 10.11b".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1 + x)^2 + (1 + y)^2 < 1/4 -> [{x'=1+x+x^2+x^3+2*y+2*x^2*y, y'=-y+2*x*y+x^2*y+2*x*y^2}@invariant((1+x^2>=0&y<=0), (1+x^2>=0), (y<=0))]!(y >= 1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 5_1_ii".

Description "Exercise 5.1 ii".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -1 & x < 0 & y < 0 & y >= -1 -> [{x'=-1+x^2+y^2, y'=5*(-1+x*y)}@invariant(-1+x+y<=0)]!(x + y > 1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 5_2_ii".

Description "Exercise 5.2 ii".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -4/5 & x < -1/3 & y < 0 & y >= -1 -> [{x'=2*x-2*x*y, y'=-x^2+2*y+y^2}]!((x = 0 & y = 0) | x + y > 1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 5_13".

Description "Equation (5.13)".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -1/2 & x < 0 & y < 0 & y >= -1/2 -> [{x'=y, y'=2*(-1-y)*y}@invariant(-2+y<=0)]!(y>2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 10_9".

Description "Exercise 10.9".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1 + x)^2 + (1 + y)^2 < 1/4 ->
  [{x'=x^4+2*x*y^2-6*x^2*y^2+y^4+x*(x^2-y^2), y'=2*x^2*y-4*x^3*y+4*x*y^3-y*(x^2-y^2)}
   @invariant(y<=0)
  ]!(y>=1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 10_11".

Description "Exercise 10.11".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + y^2 < 1/4 ->
  [{x'=-70-100*x+70*x^2+100*x^3-200*y+200*x^2*y, y'=146*x+100*y+140*x*y+100*x^2*y+200*x*y^2}
   @invariant((-1+x<=0&1+x>=0), (-1+x<=0), (1+x>=0))
  ]!(2*x >= 3 | x <= -3/2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 1_11b".

Description "Exercise 1.11b".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a > -1/2 & a < -1/3 & y < 0 & y >= -1/2 -> [{a'=2*a-a^5-a*y^4, y'=y-a^2*y-y^3}@invariant(-5+a^2+y^2<=0)]!(a^2 + y^2 > 5)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 10_15_i".

Description "Exercise 10.15".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -1 & x < -3/4 & y <= 3/2 & y >= 1 ->
  [{x'=-42*x^7+50*x^2*y+156*x^3*y+258*x^4*y-46*x^5*y+68*x^6*y+20*x*y^6-8*y^7,
    y'=y*(1110*x^6-3182*x^4*y-220*x^5*y+478*x^3*y^3+487*x^2*y^4-102*x*y^5-12*y^6)}
   @invariant((y>=0&-1+x+-1*y<=0), (y>=0), (-1+x+-1*y<=0))
  ]!(x > 1 + y)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 10_15_ii".

Description "Exercise 10.15 (modified)".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -1 & x < -1/2 & y <= -1/10 & y >= -3/10 ->
  [{x'=315*x^7+477*x^6*y-113*x^5*y^2+301*x^4*y^3-300*x^3*y^4-192*x^2*y^5+128*x*y^6-16*y^7,
    y'=y*(2619*x^6-99*x^5*y-3249*x^4*y^2+1085*x^3*y^3+596*x^2*y^4-416*x*y^5+64*y^6)}
   @invariant((((y<=0&3*x+-2*y<=0)&7*x+-2*y<=0)&-1+x+-1*y<=0), (y<=0), (3*x+-2*y<=0), (7*x+-2*y<=0), (-1+x+-1*y<=0))
  ]!(x > 1 + y)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Dumortier Llibre Artes Ex. 5_2".

Description "Example 5.2".
Citation "Freddy Dumortier, Jaume Llibre, Joan C. Artes. Qualitative Theory of Planar Differential Systems".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -1 & x < -1/2 & y < -1/2 & y >= -1
  ->
  [{x'=y, y'=x^5-x*y}@invariant((x^5+-1*x*y+-4*x^3*y<=0&y<=0), (x^5+-1*x*y+-4*x^3*y<=0), (y<=0))]!(x + y > 1)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x+1/2<=0 & y+1/2<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Fitzhugh Nagumo Ben Sassi Girard 2".

Description "Fitzhugh-Nagumo system (modified)".
Citation "Mohamed Amin Ben Sassi, Antoine Girard, Sriram Sankaranarayanan. Iterative computation of polyhedral invariants sets for polynomial dynamical systems".
Link "10.1109/CDC.2014.7040384".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  -1<=a & a<=-0.5 & 1<=y & y<=1.5 -> [{a'=7/8+a-a^3/3-y, y'=(2*(7/10+a-(4*y)/5))/25}]!(-2.5<=a & a<=-2 & -2<=y & y<=-1.5)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`0.12152*a^4+0.22807*a^3*y+0.214*a^2*y^2-0.71222*y^4-0.27942*a^3-0.48799*a^2*y-0.2517*a*y^2-0.3366*y^3-0.21526*a^2+0.16728*a*y-0.44613*y^2+0.35541*a-0.21594*y-0.72852<=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Forsman Phd Ex 6_1 page 99".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + (-2 + y)^2 < 1/24 -> [{x'=-x+2*x^2*y, y'=-y}@invariant(2+x>=0&y>=0)]!(x <= -2 | y <= -1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Forsman Phd Ex 6_14 page 119".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + (-1 + y)^2 < 1/8 -> [{x'=-2*x+y^4, y'=-y+3*x*y^3}@invariant(y>=0)]!(y <= -1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Hamiltonian System 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (2/3 + x)^2 + y^2 <= 1/24 -> [{x'=-2*y, y'=-2*x-3*x^2}]!(x>0)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`-(x^2+x^3-y^2)<0 & x<0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Invariant 3-dim sphere".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x = 1/4 & y = 1/8 & z = 1/10 -> [{x'=x^2-x*(x^3+y^3+z^3), y'=y^2-y*(x^3+y^3+z^3), z'=z^2-z*(x^3+y^3+z^3)}]!(x > 10 | y > 5 | z <= -20)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x^2+y^2+z^2<=1`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "KeYmaera Nonlinear Diffcut".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^3 >= -1 & y^5 >= 0
  ->
  [{x'=(-3+x)^4+y^5, y'=y^2}@invariant((-1+-1*x<=0&-1*y<=0), (-1+-1*x<=0), (-1*y<=0))]!(1 + x < 0 | y < 0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "KeYmaera Nonlinear 1".

ProgramVariables.
  R x.
  R a.
End.

Problem.
  x^3 >= -1 -> [{x'=a+(-3+x)^4, a'=0 & a>=0}@invariant(-1+-1*x^3<=0)]!(x^3 < -1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Liu Zhan Zhao Emsoft11 Example 25".

Description "Example 25".
Citation "Jiang Liu, Naijun Zhan, Hengjun Zhao. Computing semi-algebraic invariants for polynomial dynamical systems. EMSOFT 2011".
Link "http://doi.acm.org/10.1145/2038642.2038659".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x + y >= 0 -> [{x'=-2*y, y'=x^2}@invariant(-1/2+-1*y < 0|-1+-1*x<=0)]!(x < -1 & y <= -1/2)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x+y>=-1`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Lotka Volterra Fourth Quadrant".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x = 0 & y = 1 -> [{x'=x*(1-y), y'=-((1-x)*y) & x>=0 & y>=0}@invariant(-1*y<=0)]!(y < 0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Man Maccallum Goriely Page 57".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + y^2 <= 1/4 -> [{x'=-y+2*x^2*y, y'=y+2*x*y^2}@invariant(-1+2*x^2<=0)]!(x > 3)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Prajna PhD Thesis 2-4-1 Page 31".

Description "Page 31".
Citation "Prajna, Stephen (2005) Optimization-based methods for nonlinear and hybrid systems verification. Dissertation (Ph.D.), California Institute of Technology.".
Link "http://resolver.caltech.edu/CaltechETD:etd-05272005-144358".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-3/2 + x)^2 + y^2 <= 1/4 -> [{x'=x, y'=-x+x^3/3-y}@invariant(x>=0)]!((1 + x)^2 + (1 + y)^2 <= 4/25)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Shimizu Morioka System".

ProgramVariables.
  R x.
  R y.
  R z.
End.

Problem.
  x = 5 & y = 3 & z = -4 -> [{x'=y, y'=x-y-x*z, z'=x^2-z}@invariant(-5+-1*z<=0)]!(z < -5)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Stable Limit Cycle 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x^2 + y^2 < 1/16
  ->
  [{x'=x-x^3+y-x*y^2, y'=-x+y-x^2*y-y^3}@invariant((-2+-1*x < 0&-2+y<=0), (-2+-1*x < 0), (-2+y<=0))]!(x <= -2 | y > 2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Stable Limit Cycle 2".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a^2 + y^2 < 1/16 -> [{a'=a-a^3+y-a*y^2, y'=-a+y-a^2*y-y^3}]!(a < -1 | y < -1 | a > 1 | y > 1)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`a^2+y^2<=1`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Strogatz Exercise 6_1_5".

Description "Exercise 6.1.5".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + 2*(-1/3 + y)^2 < 1/25 -> [{x'=x*(2-x-y), y'=x-y}]!(x >= 2 | x <= -2)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x>=0 & y > 0 & x<2`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Strogatz Exercise 6_1_9 Dipole".

Description "Exercise 6.1.9".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + y^2 < 1/25 -> [{x'=2*x*y, y'=-x^2+y^2}@invariant(x>=0)]!(x <= -2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Strogatz Example 6_3_2".

Description "Example 6.3.2".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x > -4/5 & x < -1/3 & y < 3/2 & y >= 1 ->
  [{x'=-x+x*(x^2+y^2), y'=x+y*(x^2+y^2)}@invariant((x<=0&x+x^2*y+y^3>=0), (x<=0), (x+x^2*y+y^3>=0))]!(x < -1/3 & y >= 0 & 2*y < 1 & x > -4/5)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Strogatz Exercise 6_6_1 Reversible System".

Description "Exercise 6.6.1".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + (-1/3 + y)^2 < 1/16 -> [{x'=(1-x^2)*y, y'=1-y^2}@invariant(((-1+x<=0&-2+x<=0)&1+x>=0), (-1+x<=0), (-2+x<=0), (1+x>=0))]!(x >= 2 | x <= -2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Strogatz Exercise 6_6_2 Limit Cycle".

Description "Exercise 6.6.2".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + (-1/3 + y)^2 < 1/16 -> [{x'=y, y'=-x+y*(1-x^2-y^2)}]!(x^2 + y^2 = 0 | x >= 2 | x <= -2)
End.

Tactic "Scripted proof".
  /* Invariant not generated by Pegasus */
  (implyR(1)&(dC({`x^2 + y^2 - 2 <=0 & -(x^2 + y^2 - 0.01) <= 0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Strogatz Example 6_8_3".

Description "Example 6.8.3".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  2*(-1/3 + x)^2 + y^2 < 1/16 -> [{x'=x^2*y, y'=x^2-y^2}@invariant(x>=0)]!(x <= -2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Strogatz Exercise 7_3_5".

Description "Exercise 7.3.5".
Citation "Steven H. Strogatz. Nonlinear Dynamics And Chaos: With Applications To Physics, Biology, Chemistry, And Engineering".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + (-1/3 + y)^2 < 1/16 -> [{x'=-x-y+x*(x^2+2*y^2), y'=x-y+y*(x^2+2*y^2)}]!((x = 0 & y = 0) | x <= -2 | y <= -1)
End.

Tactic "Scripted proof".
  /* invariant not generated by Pegasus */
  (implyR(1)&(dC({`-4/5 + x^2 + 2*x*y/5 + 7*y^2/5 <= 0 & x^2+y^2!=0`},1)&<( (dW(1)&QE), master)))
End.

End.

ArchiveEntry "Synthetic Example 1".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x = 0 & y = 0 -> [{x'=-1+x^2+y^2, y'=x^2}@invariant(-1*y<=0)]!(y < 0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Unstable Unit Circle 1".

ProgramVariables.
  R a.
  R y.
End.

Problem.
  a = 0 & y = 1/2 -> [{a'=-a+a^3-y+a*y^2, y'=a-y+a^2*y+y^3}@invariant(-1+a^2+y^2<=0)]!(a^2 + y^2 > 1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Unstable Unit Circle 2".

ProgramVariables.
  R b.
  R y.
End.

Problem.
  b = 1/2 & y = 1/2 -> [{b'=-b+b^3-y+b*y^2, y'=b-y+b^2*y+y^3}@invariant(-2*b^2+2*b^4+-2*y^2+4*b^2*y^2+2*y^4<=0)]!(b^2 + y^2 > 2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Unstable Unit Circle 3".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x = 0 & y = 1/2 -> [{x'=-x+x^3-y+x*y^2, y'=x-y+x^2*y+y^3 & x>=0 & y>=0}@invariant(-1+x^2+y^2<=0)]!(x^2 + y^2 > 1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Van der Pol Fourth Quadrant".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x = 0 & y = 1 -> [{x'=y, y'=-x+(1-x^2)*y & x<=0 & y>=0}@invariant((x=0&-1+y=0), (x=0), (-1+y=0))]!(x^2 + y^2 > 10)
End.

End.

ArchiveEntry "Vinograd System Chicone 1_145 Page 80".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  x = 1 & y = 0 -> [{x'=y^5+x^2*(-x+y), y'=y^2*(-2*x+y)}@invariant((-1*y<=0&y<=0), (-1*y<=0), (y<=0))]!(y > 0 | y < 0)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Wiggins Example 17_1_2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + (-1/3 + y)^2 < 1/16 -> [{x'=(2+x)*(-((1-x)*x)+y), y'=-y}@invariant(2+x>=0)]!(x <= -5/2)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Wiggins Example 18_1_2".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + (-1/3 + y)^2 < 1/16 -> [{x'=-x^6-x*y, y'=x^2-y}@invariant(-1+-1*y < 0)]!(y <= -1)
End.

Tactic "Automated proof".
  master
End.

End.

ArchiveEntry "Wiggins Example 18_7_3_n".

ProgramVariables.
  R x.
  R y.
End.

Problem.
  (-1/3 + x)^2 + (-1/3 + y)^2 < 1/16 -> [{x'=-x+2*y+x^2*y+x^4*y^5, y'=-y-x^4*y^6+x^8*y^9}]!(x <= -1 | y <= -1)
End.

Tactic "Scripted proof".
  /* invariant not generated by Pegasus */
  (implyR(1)&(dC({`y>=0 & x>0`},1)&<( (dW(1)&QE), master)))
End.

End.
